package com.java.project3.controller;

import com.java.project3.domain.Khoa;
import com.java.project3.dto.KhoaDTO;
import com.java.project3.dto.Page;
import com.java.project3.dto.UserDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchResDto;
import com.java.project3.repository.KhoaRepository;
import com.java.project3.service.KhoaService;
import com.java.project3.utils.PageUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;

@Controller
public class KhoaController {
    @Autowired
    KhoaService khoaService;


    @GetMapping("/khoa/{pageIndex}/{pageSize}")
    public String list(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                       @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                       Model model) {
//        khoaId = 1L;
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = khoaService.listLop(pageIndex, pageSize);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "admin/khoa";
    }

    @GetMapping("/search_khoa")
    public String aca(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                     @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                     @RequestParam(value = "search", required = false) String search,
                     Model model) {
        ResponseDto responseDto = khoaService.searchKhoa(pageIndex,pageSize,search);
        SearchResDto searchResDto = new SearchResDto();
        if (responseDto.getObject() !=null){
            searchResDto = (SearchResDto) responseDto.getObject();
        }else {
            searchResDto.setData(null);
        }
//        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);

//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
//        model.addAttribute("page", page);
        return "admin/khoa";
    }

    @GetMapping("/them_khoa")
    public String them_khoa(Model model) {
        model.addAttribute("khoaDto",new KhoaDTO());
        return "admin/them_khoa";
    }

    @PostMapping("/khoa_process")
    public String them_khoa(KhoaDTO khoaDTO){
        ResponseDto responseDto = khoaService.create(khoaDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/khoa";
        }
    }
    // update
    @GetMapping("/update-khoa/{id}")
    public String update(@PathVariable(name = "id")Long id ,
                         Model model) {
        ResponseDto khoa  = khoaService.findById(id);

        model.addAttribute("khoa",khoa.getObject());
        return "admin/cap_nhat_khoa";
    }

    @PutMapping("/update_khoa_process")
    public String update(KhoaDTO khoaDTO){
        ResponseDto responseDto = khoaService.update(khoaDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/khoa";
        }
    }

    @GetMapping("/delete-khoa/{id}")
    public String delete(@PathVariable(name = "id")Long id){
        ResponseDto responseDto = khoaService.delete(id);
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/khoa";
        }
    }
}
