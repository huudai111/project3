package com.java.project3.controller;

import com.java.project3.dto.KhoaDTO;
import com.java.project3.dto.LopDTO;
import com.java.project3.dto.Page;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchResDto;
import com.java.project3.service.KhoaService;
import com.java.project3.service.LopService;
import com.java.project3.utils.PageUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
public class LopController {
    @Autowired
    LopService lopService;
    @Autowired
    KhoaService khoaService;

    @GetMapping("/lop/{pageIndex}/{pageSize}")
    public String list(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                       @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                       @RequestParam(value = "khoaId", required = false) Long khoaId,
                       Model model) {

        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = lopService.listLop(pageIndex, pageSize, khoaId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "admin/lop";
    }

    @GetMapping("/search_lop")
    public String qaa(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                     @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                     @RequestParam(value = "search", required = false) String search,
                     Model model) {
        ResponseDto responseDto = lopService.searchLop(pageIndex,pageSize,search);
        SearchResDto searchResDto = new SearchResDto();
        if (responseDto.getObject() !=null){
            searchResDto = (SearchResDto) responseDto.getObject();
        }else {
            searchResDto.setData(null);
        }
//        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);

//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
//        model.addAttribute("page", page);
        return "admin/lop";
    }

    @GetMapping("/them_lop")
    public String them_khoa(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                            @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                            Model model) {
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = khoaService.listLop(pageIndex, pageSize);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("lopDto", new LopDTO());
        return "admin/them_lop";
    }

    @PostMapping("/lop_process")
    public String them_khoa(@ModelAttribute("lopDto") LopDTO lopDto, ModelMap modelMap) {
        modelMap.addAttribute("lopDto", lopDto);
        ResponseDto responseDto = lopService.create(lopDto);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/lop";
        }
    }

    @GetMapping("/delete-lop/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        ResponseDto responseDto = lopService.delete(id);
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/lop";
        }
    }

    // update
    @GetMapping("/update-lop/{id}")
    public String update(@PathVariable(name = "id") Long id,
                         Model model) {
        ResponseDto lop = lopService.findById(id);

        model.addAttribute("lop", lop.getObject());
        return "admin/cap_nhat_lop";
    }

    @PutMapping("/update_lop_process")
    public String update(LopDTO lopDTO) {
        ResponseDto responseDto = lopService.update(lopDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/lop";
        }
    }
}
