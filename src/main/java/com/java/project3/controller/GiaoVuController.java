package com.java.project3.controller;

import com.java.project3.domain.GiaoVien;
import com.java.project3.dto.*;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchResDto;
import com.java.project3.repository.DoAnRepository;
import com.java.project3.repository.GiaoVienRepository;
import com.java.project3.repository.LopRepository;
import com.java.project3.repository.NhomRepository;
import com.java.project3.service.*;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Controller
public class GiaoVuController {
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    CustomerUserDetailService customerUserDetailService;
    @Autowired
    LopRepository lopRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    KhoaService khoaService;
    @Autowired
    NhomRepository nhomRepository;
    @Autowired
    HuongDanDoAnService huongDanDoAnService;
    @Autowired
    DoAnService doAnService;
    @Autowired
    GiaoVienRepository giaoVienRepository;

    @GetMapping("/giao-vien/{pageIndex}/{pageSize}")
    public String list(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                       @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                       @RequestParam(value = "id", required = false) Long khoaId,
                       Model model) {
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = giaoVienService.listGiaoVien(pageIndex, pageSize, khoaId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "admin/giaovu";
    }

    @GetMapping("/them_giao_vien")
    public String them_giao_vien(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                                 @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                                 Model model) {
        model.addAttribute("giaoVien",new GiaoVienDTO());
        ResponseDto responseDto = khoaService.listLop(pageIndex, pageSize);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();

        model.addAttribute("findAll", searchResDto.getData());

        return "admin/them_giaovu";
    }

    @GetMapping("/delete-giao-vien/{id}")
    public String delete(@PathVariable(name = "id")Long id){
        ResponseDto responseDto = giaoVienService.delete(id);
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/giao-vien";
        }
    }

    @PostMapping("/giao_vien_process")
    public String them_giao_vien(GiaoVienDTO giaoVienDTO){
        ResponseDto responseDto = giaoVienService.create(giaoVienDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/giao-vien";
        }
    }

    @GetMapping("/giao_vu/home/{id}")
    public String home(@PathVariable(name = "id")Long id ,
                       Model model) {

        GiaoVien giaoVien = giaoVienRepository.findByUserId(id);
        Long doan = ((long)0);
        if(giaoVien == null){
            doan = ((long)0);
        }else {
            doan = huongDanDoAnService.count(giaoVien.getId());
        }
        Long nhom = nhomRepository.count();
        ResponseDto e = doAnService.dem1(0, 300);
        SearchResDto searchResDto = new SearchResDto();
        if (e.getObject() instanceof SearchResDto) {
            searchResDto = (SearchResDto) e.getObject();
        }
        List<DoAnDTO> list = (ArrayList) searchResDto.getData();
        Map<String, Integer> graphData = new TreeMap<>();
        for (var i : list) {
            Integer soLieu = doAnRepository.countByThang(i.getThang());
            if (i.getThang() == 1) {
                graphData.put("T1", soLieu);
            } else {
                graphData.put("T1", 0);
            }
            if (i.getThang() == 2) {
                graphData.put("T2", soLieu);
            } else {
                graphData.put("T2", 0);
            }
            if (i.getThang() == 3) {
                graphData.put("T3", soLieu);
            } else {
                graphData.put("T3", 0);
            }
            if (i.getThang() == 4) {
                graphData.put("T4", soLieu);
            } else {
                graphData.put("T4", 0);
            }
            if (i.getThang() == 5) {
                graphData.put("T5", soLieu);
            } else {
                graphData.put("T5", 0);
            }
            if (i.getThang() == 6) {
                graphData.put("T6", soLieu);
            } else {
                graphData.put("T6", 0);
            }
            if (i.getThang() == 12) {
                graphData.put("T12", soLieu);
            } else {
                graphData.put("T12", 0);
            }
            if (i.getThang() == 7) {
                graphData.put("T7", soLieu);
            } else {
                graphData.put("T7", 0);
            }
            if (i.getThang() == 8) {
                graphData.put("T8", soLieu);
            } else {
                graphData.put("T8", 0);
            }
            if (i.getThang() == 9) {
                graphData.put("T9", soLieu);
            } else {
                graphData.put("T9", 0);
            }
            if (i.getThang() == 10) {
                graphData.put("T10", soLieu);
            } else {
                graphData.put("T10", 0);
            }
            if (i.getThang() == 11) {
                graphData.put("T11", soLieu);
            } else {
                graphData.put("T11", 0);
            }

        }
        model.addAttribute("nhom",nhom);
        model.addAttribute("doan",doan);
        model.addAttribute("id",id);
        //dem cho giao vien
        model.addAttribute("chartData", graphData);
        return "/giao_vu/home";
    }

    @GetMapping("/update-giaovien/{id}")
    public String update(@PathVariable(name = "id")Long id ,
                         Model model) {
        ResponseDto giaovien  = giaoVienService.findById(id);

        model.addAttribute("giaovien",giaovien.getObject());
        return "admin/cap_nhat_giaovu";
    }

    @PutMapping("/update_giaovien_process")
    public String update(GiaoVienDTO giaoVienDTO){
        ResponseDto responseDto = giaoVienService.update(giaoVienDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/giao-vien";
        }
    }
}
