package com.java.project3.controller;

import com.java.project3.domain.DanhGiaNHom;
import com.java.project3.domain.GiaoVien;
import com.java.project3.domain.HuongDanDoAn;
import com.java.project3.dto.*;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchResDto;
import com.java.project3.repository.DanhGiaNhomRepository;
import com.java.project3.repository.GiaoVienRepository;
import com.java.project3.repository.HuongDanDoAnRepository;
import com.java.project3.service.*;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class DoAnController {
    @Autowired
    DoAnService doAnService;
    @Autowired
    KhoaService khoaService;
    @Autowired
    LopService lopService;
    @Autowired
    DanhGiaNhomService danhGiaNhomService;
    @Autowired
    HuongDanDoAnRepository huongDanDoAnRepository;
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    NhomService nhomService;
    @Autowired
    GenIdService genIdService;
    @Autowired
    DanhGiaNhomRepository danhGiaNhomRepository;

    @GetMapping("/do-an/{pageIndex}/{pageSize}")
    public String list(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                       @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                       @RequestParam(value = "khoaId", required = false) Long khoaId,
                       Model model) {
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = doAnService.listGiaoVien(pageIndex, pageSize, khoaId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "admin/do_an";
    }

    @GetMapping("/danh_gia/{pageIndex}/{pageSize}/{id}")
    public String danhGia(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                       @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                       @PathVariable(value = "id", required = false) Long id,
                       Model model) {
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = danhGiaNhomService.LocList(pageIndex, pageSize, id);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "admin/danh_gia";
    }
    @GetMapping("/them_danh_gia/{nhom_id}/{id}")
    public String them_danh_gia(@PathVariable(value = "nhom_id") Long nhomId,
                                @PathVariable(value = "id") Long id,
                             Model model) {
        DanhGiaNHom danhGiaNHom = new DanhGiaNHom();
        danhGiaNHom.setNhomId(id);
        danhGiaNHom.setId(genIdService.nextId());
        danhGiaNHom.setGiaoVienId(nhomId);
        danhGiaNHom.setIsDeleted(false);
        danhGiaNhomRepository.save(danhGiaNHom);
//        model.addAttribute("id",d);
        model.addAttribute("danhGiaDto",danhGiaNHom);
        return "admin/them_danh_gia";
    }

    @PutMapping("/danh_gia_process")
    public String them_danh_gia(DanhGiaNhomDTO danhGiaNhomDTO){
        ResponseDto responseDto = danhGiaNhomService.update(danhGiaNhomDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            DanhGiaNhomDTO danhGiaNhomDTO1 = (DanhGiaNhomDTO) responseDto.getObject();
            return "redirect:/List_danh_gia/" + danhGiaNhomDTO1.getId();
        }
    }


    @GetMapping("/List_danh_gia/{id}")
    public String danhGia(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                          @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                          @RequestParam(value = "nhomId", required = false) Long nhomId,
                          @PathVariable(value = "id", required = false) Long id,
                          Model model) {
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = nhomService.listSinhVien(pageIndex, pageSize);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);
        model.addAttribute("id",id);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "admin/danh_gia";
    }

    @GetMapping("/do-an-giao-vien/{pageIndex}/{pageSize}/{id}")
    public String as(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                       @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                       @RequestParam(value = "khoaId", required = false) Long khoaId,
                        @PathVariable(value = "id", required = false) Long id,
                     @PathVariable(value = "gvId", required = false) Long gvId,

                     Model model) {
        GiaoVien giaoVien = giaoVienRepository.findByUserId(id);
        List<HuongDanDoAn> huongDanDoAn = huongDanDoAnRepository.findByGiaoVienId(giaoVien.getId());
        List<Long> doAnId =huongDanDoAn.stream().map(x -> x.getDoAnId()).collect(Collectors.toList());
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = doAnService.listdoAn(pageIndex, pageSize, doAnId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "giao_vu/ds_do";
    }
    @GetMapping("/delete-doan/{id}")
    public String delete(@PathVariable(name = "id")Long id){
        ResponseDto responseDto = doAnService.delete(id);
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/do-an";
        }
    }

    @GetMapping("/search_do_an")
    public String aa(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                     @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                     @RequestParam(value = "search", required = false) String search,
                     Model model) {
        ResponseDto responseDto = doAnService.searchDoAn(pageIndex,pageSize,search);
        SearchResDto searchResDto = new SearchResDto();
        if (responseDto.getObject() !=null){
            searchResDto = (SearchResDto) responseDto.getObject();
        }else {
            searchResDto.setData(null);
        }
//        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);

//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
//        model.addAttribute("page", page);
        return "admin/do_an";
    }

    @GetMapping("/search_do_an_giao_vien")
    public String aac(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                     @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                     @RequestParam(value = "search", required = false) String search,
                     Model model) {
        ResponseDto responseDto = doAnService.searchDoAn(pageIndex,pageSize,search);
        SearchResDto searchResDto = new SearchResDto();
        if (responseDto.getObject() !=null){
            searchResDto = (SearchResDto) responseDto.getObject();
        }else {
            searchResDto.setData(null);
        }
//        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);

//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
//        model.addAttribute("page", page);
        return "giao_vu/ds_do";
    }


    @GetMapping("/them_do_an")
    public String them_do_an(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                             @PathVariable(value = "pageIndex", required = false) Integer pageIndex,

                             Model model) {
        ResponseDto all = giaoVienService.all();
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = khoaService.listLop(pageIndex, pageSize);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);
//        Page page1 = PageUltil.setDefault(pageIndex, pageSize);
//        ResponseDto responseDto1 = lopService.listLop(pageIndex, pageSize,khoaId);
//        SearchResDto searchResDto1 = (SearchResDto) responseDto1.getObject();
//        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

//        model.addAttribute("findAllLop", searchResDto1.getData());
        model.addAttribute("all",all.getObject());
        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("doAnDto",new DoAnDTO());
        return "admin/them_do_an";
    }

    @PostMapping("/do_an_process")
    public String them_do_an(DoAnDTO doAnDTO){
        ResponseDto responseDto = doAnService.create(doAnDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/do-an";
        }
    }

    @GetMapping("/update-doan/{id}")
    public String update(@PathVariable(name = "id")Long id ,
                         Model model) {
        ResponseDto doan  = doAnService.findById(id);

        model.addAttribute("doan",doan.getObject());
        return "admin/cap_nhat_do_an";
    }

    @PutMapping("/update_doan_process")
    public String update(DoAnDTO doAnDTO){
        ResponseDto responseDto = doAnService.update(doAnDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/do-an";
        }
    }
}
