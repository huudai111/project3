package com.java.project3.controller;

import com.java.project3.domain.User;
import com.java.project3.dto.DoAnDTO;
import com.java.project3.dto.UserDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchResDto;
import com.java.project3.repository.DoAnRepository;
import com.java.project3.repository.LopRepository;
import com.java.project3.repository.UserRepository;
import com.java.project3.service.*;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
public class HomeController {
    @Autowired
    CustomerUserDetailService customerUserDetailService;
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    LopRepository lopRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DoAnService doAnService;
    @Autowired
    KhoaService khoaService;


    @GetMapping("/home/{id}")
    public String home(@PathVariable(name = "id") Long id,
                       Model model) {
        Long a = customerUserDetailService.cout();
        Long demGiaoVien = giaoVienService.demGiaoVien();
        ;
        Long demLop = lopRepository.count();
        Long demDoAn = doAnRepository.count();
        ResponseDto e = doAnService.dem1(0, 300);
        SearchResDto searchResDto = new SearchResDto();
        if (e.getObject() instanceof SearchResDto) {
            searchResDto = (SearchResDto) e.getObject();
        }
        List<DoAnDTO> list = (ArrayList) searchResDto.getData();
        Map<String, Integer> graphData = new TreeMap<>();
        for (var i : list) {
            Integer soLieu = doAnRepository.countByThang(i.getThang());
            if (i.getThang() == 1) {
                graphData.put("T1", soLieu);
            } else {
                graphData.put("T1", 0);
            }
            if (i.getThang() == 2) {
                graphData.put("T2", soLieu);
            } else {
                graphData.put("T2", 0);
            }
            if (i.getThang() == 3) {
                graphData.put("T3", soLieu);
            } else {
                graphData.put("T3", 0);
            }
            if (i.getThang() == 4) {
                graphData.put("T4", soLieu);
            } else {
                graphData.put("T4", 0);
            }
            if (i.getThang() == 5) {
                graphData.put("T5", soLieu);
            } else {
                graphData.put("T5", 0);
            }
            if (i.getThang() == 6) {
                graphData.put("T6", soLieu);
            } else {
                graphData.put("T6", 0);
            }
            if (i.getThang() == 12) {
                graphData.put("T12", soLieu);
            } else {
                graphData.put("T12", 0);
            }
            if (i.getThang() == 7) {
                graphData.put("T7", soLieu);
            } else {
                graphData.put("T7", 0);
            }
            if (i.getThang() == 8) {
                graphData.put("T8", soLieu);
            } else {
                graphData.put("T8", 0);
            }
            if (i.getThang() == 9) {
                graphData.put("T9", soLieu);
            } else {
                graphData.put("T9", 0);
            }
            if (i.getThang() == 10) {
                graphData.put("T10", soLieu);
            } else {
                graphData.put("T10", 0);
            }
            if (i.getThang() == 11) {
                graphData.put("T11", soLieu);
            } else {
                graphData.put("T11", 0);
            }

        }
        model.addAttribute("count", a);
        model.addAttribute("countB", demGiaoVien);
        model.addAttribute("countLop", demLop);
        model.addAttribute("countDoAn", demDoAn);
        model.addAttribute("id", id);

        //dem cho giao vien
        model.addAttribute("chartData", graphData);

//        ModelAndView modelAndView = new ModelAndView("home");
        return "/home";
    }


    @GetMapping("/thong-tin/{id}")
    public String thongtin(@PathVariable(name = "id") Long id,
                           Model model) {

        ResponseDto responseDto = customerUserDetailService.findById(id);

        UserDTO userDTO = (UserDTO) responseDto.getObject();
        model.addAttribute("user", userDTO);
        return "/thong-tin";
    }

    // login
    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("userDto", new UserDTO());
        return "/login";
    }

    @PostMapping("/tesst")
    public String tesst(UserDTO userDTO) throws MessagingException {
        ResponseDto responseDto = customerUserDetailService.login(userDTO);
        com.java.project3.domain.User user = null;
        if(responseDto.getObject() != null){
            user = (User) ((Optional) responseDto.getObject()).get();
            responseDto.toString();
        }

        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/login";
        } else {
            if (user != null) {
                if (user.getCapDo() == true) {
                    return "redirect:/home/" + user.getId();
                } else {
                    return "redirect:/giao_vu/home/" + user.getId();
                }
            }else {
                return "redirect:/login";
            }
        }
    }
    //end

    // sigup

    // gọi đến view đăng ký truyền bản lưu mẫu gọi chung là "...DTO"
    @GetMapping("/signup")
    public String signup(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                         @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                         Model model) {
        ResponseDto responseDto = khoaService.listLop(pageIndex, pageSize);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("userDto", new UserDTO());

        return "signup";
    }


    // xử lý đăng ký
    @PostMapping("/signup_process")
    public String signup(UserDTO userDTO) throws MessagingException {
        ResponseDto responseDto = customerUserDetailService.signup(userDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/login";
        }
    }

    //end
    @GetMapping("/admin/thong_ke")
    public String thong_ke(Model model) {
        Long a = customerUserDetailService.cout();
        Long demGiaoVien = giaoVienService.demGiaoVien();
        ;
        Long demLop = lopRepository.count();
        Long demDoAn = doAnRepository.count();
        model.addAttribute("count", a);
        model.addAttribute("countB", demGiaoVien);
        model.addAttribute("countLop", demLop);
        model.addAttribute("countDoAn", demDoAn);

        return "/admin/thong_ke";
    }

    @GetMapping("/logout")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

    @GetMapping("/admin/giaovu")
    public ModelAndView giaovu() {
        ModelAndView modelAndView = new ModelAndView("admin/giaovu");
        return modelAndView;
    }

    @GetMapping("/admin/sinh_vien")
    public ModelAndView sinhvien() {
        ModelAndView modelAndView = new ModelAndView("admin/sinh_vien");
        return modelAndView;
    }


    @GetMapping("/admin/lop")
    public ModelAndView lop() {
        ModelAndView modelAndView = new ModelAndView("admin/lop");
        return modelAndView;
    }

    @GetMapping("/admin/do_an")
    public ModelAndView do_an() {
        ModelAndView modelAndView = new ModelAndView("admin/do_an");
        return modelAndView;
    }

    @GetMapping("/admin/de_tai")
    public ModelAndView de_tai() {
        ModelAndView modelAndView = new ModelAndView("admin/de_tai");
        return modelAndView;
    }

    @GetMapping("/admin/khoa")
    public ModelAndView khoa() {
        ModelAndView modelAndView = new ModelAndView("admin/khoa");
        return modelAndView;
    }

    @GetMapping("/admin/them_giaovu")
    public ModelAndView them_giaovu() {
        ModelAndView modelAndView = new ModelAndView("admin/them_giaovu");
        return modelAndView;
    }

    @GetMapping("/admin/cap_nhat_giaovu")
    public ModelAndView cap_nhat_giaovu() {
        ModelAndView modelAndView = new ModelAndView("admin/cap_nhat_giaovu");
        return modelAndView;
    }

    @GetMapping("/admin/them_SV")
    public ModelAndView them_SV() {
        ModelAndView modelAndView = new ModelAndView("admin/them_SV");
        return modelAndView;
    }

    @GetMapping("/admin/cap_nhat_SV")
    public ModelAndView cap_nhat_SV() {
        ModelAndView modelAndView = new ModelAndView("admin/cap_nhat_SV");
        return modelAndView;
    }


    @GetMapping("/admin/cap_nhat_khoa")
    public ModelAndView cap_nhat_khoa() {
        ModelAndView modelAndView = new ModelAndView("admin/cap_nhat_khoa");
        return modelAndView;
    }

    @GetMapping("/admin/them_de_tai")
    public ModelAndView them_de_tai() {
        ModelAndView modelAndView = new ModelAndView("them_danh_gia");
        return modelAndView;
    }

    @GetMapping("/admin/cap_nhat_de_tai")
    public ModelAndView cap_nhat_de_tai() {
        ModelAndView modelAndView = new ModelAndView("admin/cap_nhat_de_tai");
        return modelAndView;
    }

    @GetMapping("/admin/cap_nhat_do_an")
    public ModelAndView cap_nhat_do_an() {
        ModelAndView modelAndView = new ModelAndView("admin/cap_nhat_do_an");
        return modelAndView;
    }

    @GetMapping("/admin/them_do_an")
    public ModelAndView them_do_an() {
        ModelAndView modelAndView = new ModelAndView("admin/them_do_an");
        return modelAndView;
    }

    @GetMapping("/admin/chi_tiet_lop")
    public ModelAndView chi_tiet_lop() {
        ModelAndView modelAndView = new ModelAndView("danh_gia");
        return modelAndView;
    }

    @GetMapping("/admin/chi_tiet_nhom")
    public ModelAndView chi_tiet_nhom() {
        ModelAndView modelAndView = new ModelAndView("admin/chi_tiet_nhom");
        return modelAndView;
    }


}
