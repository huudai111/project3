package com.java.project3.controller;

import com.java.project3.domain.SinhVien;
import com.java.project3.dto.*;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchResDto;
import com.java.project3.repository.NhomSinhVienRepository;
import com.java.project3.repository.SinhVienRepository;
import com.java.project3.service.*;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SinhVienCotroller {
    @Autowired
    SinhVienService sinhVienService;
    @Autowired
    ImportExcelSinhVienService importExcelSinhVienService;
    @Autowired
    FileStorageService fileStorageService;
    @Autowired
    CustomerUserDetailService customerUserDetailService;
    @Autowired
    KhoaService khoaService;
    @Autowired
    NhomSinhVienService nhomSinhVienService;
    @Autowired
    DoAnService doAnService;
    @Autowired
    NhomSinhVienRepository nhomSinhVienRepository;
    @Autowired
    SinhVienRepository sinhVienRepository;


    @GetMapping("/sinh_vien/{pageIndex}/{pageSize}")
    public String list(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                       @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                       @RequestParam(value = "khoaId", required = false) Long khoaId,
                       Model model) {
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = sinhVienService.listSinhVien(pageIndex, pageSize, khoaId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        return "admin/sinh_vien";
    }

    @GetMapping("/search_sinh_vien")
    public String aa(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                     @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                     @RequestParam(value = "search", required = false) String search,
                     Model model) {
        ResponseDto responseDto = sinhVienService.searchSinhVien(pageIndex, pageSize, search);
        SearchResDto searchResDto = new SearchResDto();
        if (responseDto.getObject() != null) {
            searchResDto = (SearchResDto) responseDto.getObject();
        } else {
            searchResDto.setData(null);
        }
//        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);

//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
//        model.addAttribute("page", page);
        return "admin/sinh_vien";
    }

    @GetMapping("/search_sinh_vien_giao_vien")
    public String aab(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                      @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                      @RequestParam(value = "search", required = false) String search,
                      Model model) {
        ResponseDto responseDto = sinhVienService.searchSinhVien(pageIndex, pageSize, search);
        SearchResDto searchResDto = new SearchResDto();
        if (responseDto.getObject() != null) {
            searchResDto = (SearchResDto) responseDto.getObject();
        } else {
            searchResDto.setData(null);
        }
//        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);

//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
//        model.addAttribute("page", page);
        return "giao_vu/sinh_vien";
    }

    @GetMapping("/sinh_vien_giao_vien/{pageIndex}/{pageSize}/{id}")
    public String a(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                    @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                    @RequestParam(value = "khoaId", required = false) Long khoaId,
                    @PathVariable(value = "id", required = false) Long id,
                    Model model) {
        Page page = PageUltil.setDefault(pageIndex, pageSize);
        ResponseDto responseDto = sinhVienService.listSinhVien(pageIndex, pageSize, khoaId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        page = PageUltil.format(pageIndex, searchResDto.getTotalPages(), pageSize);
//        page = PageUltil.format(pageIndex,pageSize);

        model.addAttribute("findAll", searchResDto.getData());
        model.addAttribute("page", page);
        model.addAttribute("id", id);
        return "giao_vu/sinh_vien";
    }

    @GetMapping("/testList")
    public ModelAndView testList() {
        ModelAndView modelAndView = new ModelAndView("testList");
        return modelAndView;
    }

    @GetMapping("/delete-sinh-vien/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        ResponseDto responseDto = sinhVienService.delete(id);
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/sinh_vien";
        }
    }

    @GetMapping("/update-sinh-vien/{id}")
    public String update(@PathVariable(name = "id") Long id,
                         Model model) {
        ResponseDto sinhvien = sinhVienService.findById(id);

        model.addAttribute("sinhvien", sinhvien.getObject());
        return "admin/cap_nhat_SV";
    }

    @PutMapping("/update_sinhvien_process")
    public String update(SinhVienDTO sinhVienDTO) {
        ResponseDto responseDto = sinhVienService.update(sinhVienDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/sinh_vien";
        }
    }

    @GetMapping("/export-template")
    public ResponseEntity<Resource> exportExcel(HttpServletRequest request) throws IOException {
        String locale = LocaleContextHolder.getLocale().toString();
        String fileName = "";
        if (locale.equals("vi")) {
            fileName = "them_sinh_vien.xlsx";
        } else {
            fileName = "add_students.xlsx";
        }
        Resource resource = fileStorageService.loadTemplateFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            System.out.println("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(resource);
    }

    private final String UPLOAD_DIR = "./uploads/";

    @PostMapping("/upload")
    @ResponseBody
    public Object reviewImportExcel(
            @RequestParam("file") MultipartFile multipartFile,
            RedirectAttributes redirectAttributes
    ) throws IOException, MessagingException, ParseException {
        ResponseDto responseDto = importExcelSinhVienService.CreateDataFromExcel(multipartFile);
        if (responseDto.getStatus() == 204) {
            return false;
        }
        return responseDto.getObject();
    }

    @GetMapping("/them_sinh_vien")
    public String them_sinh_vien(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                                 @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                                 Model model) {
        ResponseDto responseDto = khoaService.listLop(pageIndex, pageSize);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();

        model.addAttribute("sinhVienDto", new CreateSinhVienDTO());
        model.addAttribute("findAll", searchResDto.getData());
        return "admin/them_SV";
    }

    @PostMapping("/sinh_vien_process")
    public String them_sinh_vien(CreateSinhVienDTO createSinhVienDTO) {
        ResponseDto responseDto = sinhVienService.create(createSinhVienDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/sinh_vien";
        }
    }

    @GetMapping("/admin/nhom_SV/{pageIndex}/{pageSize}")
    public String nhom_SV(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                          @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                          @PathVariable(value = "nhom_id", required = false) Long nhomId,
                          Model model) {
        ResponseDto responseDto = nhomSinhVienService.listNhomSinhVien(pageIndex, pageSize, nhomId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        List<NhomSinhVienDTO> alo = (List<NhomSinhVienDTO>) searchResDto.getData();
//        List<NhomSinhVienDTO> nhom = new NhomSinhVienDTO();
        List<NhomSinhVienDTO> list = new ArrayList<>();
        for (var i : alo) {
            if (i.getTruongNhom() == true) {
                list.add(i);
            }
        }
        model.addAttribute("nhom", list);
        return "admin/nhom_SV";
    }
    @GetMapping("/giao_vu/nhom_SV/{pageIndex}/{pageSize}/{id}")
    public String nhom_SVs(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                          @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                          @PathVariable(value = "nhom_id", required = false) Long nhomId,
                           @PathVariable(value = "id", required = false) Long id,
                          Model model) {
        ResponseDto responseDto = nhomSinhVienService.listNhomSinhVien(pageIndex, pageSize, nhomId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
        List<NhomSinhVienDTO> alo = (List<NhomSinhVienDTO>) searchResDto.getData();
//        List<NhomSinhVienDTO> nhom = new NhomSinhVienDTO();
        List<NhomSinhVienDTO> list = new ArrayList<>();
        for (var i : alo) {
            if (i.getTruongNhom() == true) {
                list.add(i);
            }
        }
        model.addAttribute("nhom", list);
        model.addAttribute("id", id);
        return "giao_vu/nhom_SV";
    }


    @GetMapping("/admin/them_nhom_SV")
    public String them_nhom_SV(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                               @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                               @PathVariable(value = "lop_id", required = false) Long lopId,
                               Model model) {
        ResponseDto responseDto = doAnService.listGiaoVien(pageIndex, pageSize, lopId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();

        model.addAttribute("nhom", new NhomSinhVienDTO());
        model.addAttribute("findAll", searchResDto.getData());

        return "/admin/them_nhom_SV";
    }

    @GetMapping("/chi_tiet_nhom/{nhomId}")
    public String chi(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                      @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                      @PathVariable(value = "nhom_id", required = false) Long nhomId,
                      Model model) {
        ResponseDto responseDto = nhomSinhVienService.listNhomSinhVien(pageIndex, pageSize, nhomId);
        SearchResDto searchResDto = (SearchResDto) responseDto.getObject();
//        List<NhomSinhVienDTO> alo = (List<NhomSinhVienDTO>) searchResDto.getData();
//        List<NhomSinhVienDTO> nhom = new NhomSinhVienDTO();
//        List<NhomSinhVienDTO> list = new ArrayList<>();
//        for (var i : alo) {
//            if (i.getTruongNhom() == true) {
//                list.add(i);
//            }
//        }
        model.addAttribute("nhom", searchResDto.getData());
        return "admin/chi_tiet_nhom";
    }

    @PostMapping("/nhom_process")
    public String nhom(NhomSinhVienDTO nhomSinhVienDTO) {
        ResponseDto responseDto = nhomSinhVienService.create(nhomSinhVienDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/admin/nhom_SV";
        }
    }

    @GetMapping("/update-nhom/{id}")
    public String updateNhom(@PathVariable(value = "pageSize", required = false) Integer pageSize,
                             @PathVariable(value = "pageIndex", required = false) Integer pageIndex,
                             @RequestParam(value = "lopId", required = false) Long lopId,
                             @PathVariable(name = "id") Long id,
                             Model model) {
        ResponseDto nhom = nhomSinhVienService.findById(id);
        ResponseDto doAn = doAnService.listGiaoVien(pageIndex, pageSize, lopId);
        SearchResDto searchResDto = (SearchResDto) doAn.getObject();

        model.addAttribute("alo", searchResDto.getData());
        model.addAttribute("nhom", nhom.getObject());
        return "admin/cap_nhat_nhom_SV";
    }

    @PutMapping("/update_nhom_process")
    public String updateNhom(NhomSinhVienDTO nhomSinhVienDTO) {
        ResponseDto responseDto = nhomSinhVienService.update(nhomSinhVienDTO);
        // status = 200 là đăng ký thành công
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/admin/nhom_SV";
        }
    }

    @GetMapping("/delete-nhom/{id}")
    public String deleteNhom(@PathVariable(name = "id") Long id) {
        ResponseDto responseDto = nhomSinhVienService.delete(id);
        if (responseDto.getStatus() != 200) {
            // tạm tra  /
            return "redirect:/";
        } else {
            return "redirect:/admin/nhom_SV";
        }
    }

//    @GetMapping("/admin/cap_nhat_nhom_SV")
//    public ModelAndView cap_nhat_nhom_SV() {
//        ModelAndView modelAndView = new ModelAndView("admin/cap_nhat_nhom_SV");
//        return modelAndView;
//    }
}
