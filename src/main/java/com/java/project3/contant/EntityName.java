package com.java.project3.contant;

public class EntityName {
    //nhóm user
    public final static String USER = "tbl_user";
    public final static String ROLE = "tbl_role";
    public final static String USER_ROLE = "tbl_user_role";
    public final static String SINH_VIEN = "tbl_sinh_vien";
    public final static String GIAO_VIEN = "tbl_giao_vien";
    public final static String NHOM = "tbl_nhom";
    public final static String NHOM_SINH_VIEN = "tbl_nhom_sinh_vien";
    public final static String DANH_GIA_NHOM = "tbl_danh_gia_nhom";
    public final static String HUONG_DAN_DO_AN = "tbl_huong_dan_do_an";
    public final static String DO_AN = "tbl_do_an";
    public final static String DE_TAI_DO_AN = "tbl_de_tai_do_an";
    public final static String KHOA = "tbl_khoa";
    public final static String LOP = "tbl_lop";
    public final static String RESOURCE = "tbl_resource";

}
