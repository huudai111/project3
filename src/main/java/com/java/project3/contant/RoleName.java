package com.java.project3.contant;

public enum RoleName {
    GIAO_VU(null),
    ADMIN(null),;

    private RoleName parent = null;

    RoleName(RoleName parent) {
        this.parent =parent;
    }

    public static Long getRoleIdByRoleName(RoleName roleName) {
        Long roleId = null;
        switch (roleName) {
            case GIAO_VU:
                roleId = 3481555555555578L;
                break;
            case ADMIN:
                roleId = 3481666666666666L;
                break;
        }
        return roleId;
    }
}
