//package com.java.project3.configuration;
//
//import org.apache.catalina.User;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//
//@Mapper(componentModel = "spring")
//public interface UserMapper {
//
//    default UserDTO userToUserDTO(User user) {
//        return new UserDTO(user);
//    }
//
//    List<UserDTO> usersToUserDTOs(List<User> users);
//
//    @Mapping(target = "createdBy", ignore = true)
//    @Mapping(target = "createdDate", ignore = true)
//    @Mapping(target = "lastModifiedBy", ignore = true)
//    @Mapping(target = "lastModifiedDate", ignore = true)
//    @Mapping(target = "persistentTokens", ignore = true)
//    @Mapping(target = "activationKey", ignore = true)
//    @Mapping(target = "resetKey", ignore = true)
//    @Mapping(target = "resetDate", ignore = true)
//    @Mapping(target = "password", ignore = true)
//    User userDTOToUser(UserDTO userDTO);
//
//    List<User> userDTOsToUsers(List<UserDTO> userDTOs);
//
//    default User userFromId(Long id) {
//        if (id == null) {
//            return null;
//        }
//        User user = new User();
//        user.setId(id);
//        return user;
//    }
//
//    default Set<Authority> authoritiesFromStrings(Set<String> strings) {
//        return strings.stream().map(string -> {
//            Authority auth = new Authority();
//            auth.setName(string);
//            return auth;
//        }).collect(Collectors.toSet());
//    }
//}
