package com.java.project3.domain;

import com.java.project3.contant.EntityName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = EntityName.HUONG_DAN_DO_AN)
@Entity(name = EntityName.HUONG_DAN_DO_AN)
public class HuongDanDoAn {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "do_an_id")
    private Long doAnId;

    @Column(name = "giao_vien_id")
    private Long giaoVienId;

    @Column(name = "de_tai_do_an_id")
    private Long deTaiDoAnId;

    @Column(name = "trang_thai")
    private Short trangThai;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "created_by_name")
    private String createdByName;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "updated_by_name")
    private String updatedByName;
}
