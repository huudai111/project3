package com.java.project3.domain;

import com.java.project3.contant.EntityName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = EntityName.DO_AN)
@Entity(name = EntityName.DO_AN)
public class DoAn {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "khoa_id")
    private Long khoaId;

    @Column(name = "ten_khoa")
    private String tenKhoa;

    @Column(name = "lop_id")
    private Long lopId;

    @Column(name = "ten_lop")
    private String tenLop;

    @Column(name = "name")
    private String name;

    @Column(name = "noi_dung")
    private String noiDung;

    @Column(name = "hoan_thanh")
    private Short hoanThanh;

    @Column(name = "thang")
    private Integer thang;

    @Column(name = "ngay_bat_dau")
    private Date ngayBatDau;

    @Column(name = "ngay_ket_thuc")
    private Date ngayKetThuc;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "created_by_name")
    private String createdByName;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "updated_by_name")
    private String updatedByName;
}
