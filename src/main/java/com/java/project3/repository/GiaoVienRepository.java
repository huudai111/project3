package com.java.project3.repository;

import com.java.project3.domain.GiaoVien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GiaoVienRepository extends JpaRepository<GiaoVien, Long>, JpaSpecificationExecutor<GiaoVien> {
    GiaoVien findByEmail(String email);
    GiaoVien findByUserId(Long userId);
}
