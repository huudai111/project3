package com.java.project3.repository;

import com.java.project3.domain.DanhGiaNHom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DanhGiaNhomRepository extends JpaRepository<DanhGiaNHom, Long>, JpaSpecificationExecutor<DanhGiaNHom> {
}
