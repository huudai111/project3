package com.java.project3.repository;

import com.java.project3.domain.Lop;
import com.java.project3.domain.SinhVien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LopRepository extends JpaRepository<Lop, Long>, JpaSpecificationExecutor<Lop> {
    long countByKhoaId(Long khoaId);

    @Query(value = "select * from tbl_lop where lower(name) like lower(CONCAT('%',(REPLACE(:search,' ','')),'%')) ",nativeQuery = true)
    Page<Lop> searchLop(@Param("search") String search , Pageable pageable);
}
