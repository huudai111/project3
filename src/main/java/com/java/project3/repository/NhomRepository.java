package com.java.project3.repository;

import com.java.project3.domain.Nhom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NhomRepository extends JpaRepository<Nhom, Long>, JpaSpecificationExecutor<Nhom> {
Nhom findByDoAnId(Long doAnId);
}
