package com.java.project3.repository;

import com.java.project3.domain.NhomSinhVien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface NhomSinhVienRepository extends JpaRepository<NhomSinhVien, Long>, JpaSpecificationExecutor<NhomSinhVien> {
List<NhomSinhVien> findByDoAnId(Long doAnId);
}
