package com.java.project3.repository;

import com.java.project3.domain.Khoa;
import com.java.project3.domain.SinhVien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface KhoaRepository extends JpaRepository<Khoa, Long>, JpaSpecificationExecutor<Khoa> {
Khoa findByName(String name);

    @Query(value = "select * from tbl_khoa where lower(name) like lower(CONCAT('%',(REPLACE(:search,' ','')),'%')) ",nativeQuery = true)
    Page<Khoa> searchkhoa(@Param("search") String search , Pageable pageable);
}
