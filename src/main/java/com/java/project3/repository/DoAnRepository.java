package com.java.project3.repository;

import com.java.project3.domain.DoAn;
import com.java.project3.domain.SinhVien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DoAnRepository extends JpaRepository<DoAn, Long>, JpaSpecificationExecutor<DoAn> {

    @Query(value = "select * from tbl_do_an where lower(ten_khoa) like lower(CONCAT('%',(REPLACE(:search,' ','')),'%')) ",nativeQuery = true)
    Page<DoAn> searchDoAn(@Param("search") String search , Pageable pageable);

    Integer countByThang(Integer thang);
}
