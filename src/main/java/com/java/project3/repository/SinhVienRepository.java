package com.java.project3.repository;

import com.java.project3.domain.SinhVien;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SinhVienRepository extends JpaRepository<SinhVien, Long>, JpaSpecificationExecutor<SinhVien> {
    SinhVien findByMaSv(String maSv);

    long countByLopId(Long lopId);

    @Query(value = "select * from tbl_sinh_vien where lower(ten_lop) like lower(CONCAT('%',(REPLACE(:search,' ','')),'%')) ",nativeQuery = true)
    Page<SinhVien> searchSinhVien(@Param("search") String search , Pageable pageable);
}
