package com.java.project3.repository;

import com.java.project3.domain.HuongDanDoAn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface HuongDanDoAnRepository extends JpaRepository<HuongDanDoAn, Long>, JpaSpecificationExecutor<HuongDanDoAn> {
    List<HuongDanDoAn> findByGiaoVienId(Long giaoVienId);

//    List<HuongDanDoAn> findByGiaoVienId(Long giaoVienId);
}
