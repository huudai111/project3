package com.java.project3.api;


//import com.bkh.vnoip.dto.base.ResponseDto;
//import com.bkh.vnoip.dto.create.UserSignUpDto;
//import com.bkh.vnoip.security.v2.JwtTokenUtil;
//import com.bkh.vnoip.service.FileStorageService;
//import com.bkh.vnoip.service.base.ContextService;
//import com.bkh.vnoip.service.user.UserRoleService;
//import com.bkh.vnoip.service.user.UserService;
import com.java.project3.dto.UserDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.service.CustomerUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/auth")
public class ApiAuthController {
    @Autowired
    private CustomerUserDetailService customerUserDetailService;
//    @Autowired
//    private UserRoleService userRoleService;
//    @Autowired
//    ContextService contextService;
//    @Autowired
//    FileStorageService fileStorageService;
//    @Autowired
//    JwtTokenUtil jwtTokenUtil;
    @Value("${cookie.domain.parrent}")
    String parrentCookieDomain;

    @PostMapping("/signin")
    public ResponseDto login(@RequestParam(name = "username") String username,
//                             @RequestParam(name = "password") String password,
                             HttpServletResponse response) {
        ResponseDto responseDto = new ResponseDto();
        UserDetails userDetails = customerUserDetailService.loadUserByUsername(username);
        responseDto.setObject(userDetails);
        return responseDto;
    }



//    @PostMapping("/registration")
//    public ResponseDto signup(@RequestBody UserSignUpDto userSignUpDto, HttpServletRequest request) throws MessagingException {
//        return userService.createUser(userSignUpDto, request);
//    }
//
//    @PostMapping("/post-file")
//    public void postFile(@RequestParam(name = "file")MultipartFile file){
//        fileStorageService.storeTemplateFile(file);
//    }
//
//    @PostMapping("/add-user-to-role")
//    public Boolean addUserToRole(@RequestParam(name = "roleName") String roleName) {
//        return userRoleService.addUserToRole(roleName);
//    }

}
