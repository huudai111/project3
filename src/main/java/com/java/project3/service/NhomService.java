package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.DeTaiDoAn;
import com.java.project3.domain.DoAn;
import com.java.project3.domain.Nhom;
import com.java.project3.domain.User;
import com.java.project3.dto.NhomDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.DeTaiDoAnRepository;
import com.java.project3.repository.DoAnRepository;
import com.java.project3.repository.NhomRepository;
import com.java.project3.service.base.ContextService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class NhomService {
    @Autowired
    NhomRepository nhomRepository;
    @Autowired
    ContextService contextService;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    DeTaiDoAnRepository deTaiDoAnRepository;

    JMapper<Nhom, NhomDTO> toNhom;
    JMapper<NhomDTO, Nhom> toNhomDto;

    public NhomService() {
        this.toNhom = new JMapper<>(Nhom.class, NhomDTO.class);
        this.toNhomDto = new JMapper<>(NhomDTO.class, Nhom.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<Nhom> nhom = nhomRepository.findById(id);
        if (nhom.isPresent()) {
            NhomDTO nhomDTO = toNhomDto.getDestination(nhom.get());
            responseDto.setObject(nhomDTO);
        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<Nhom> nhoms = nhomRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<NhomDTO> nhomDTOS = new ArrayList<>();
        for (var nhom : nhoms) {
            NhomDTO nhomDTO = toNhomDto.getDestination(nhom);
            nhomDTOS.add(nhomDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(nhoms.getTotalPages(), nhoms.getNumber(), nhoms.getTotalElements(), nhomDTOS));

        return responseDto;
    }

    public ResponseDto listSinhVien(Integer pageIndex, Integer pageSize) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        searchReqDto.setQuery("");
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }

    public ResponseDto create(NhomDTO nhomDTO) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(nhomDTO.getDoAnId());
        Optional<DeTaiDoAn> deTaiDoAn = deTaiDoAnRepository.findById(nhomDTO.getDeTaiDoAnId());
        if (deTaiDoAn.isPresent() && doAn.isPresent()) {
            if (deTaiDoAn.get().getDoAnId().equals(doAn.get().getId())) {
                Nhom nhom = toNhom.getDestination(nhomDTO);
                nhom.setIsDeleted(false);
                Nhom result = nhomRepository.save(nhom);
                responseDto.setObject(result);
            } else {
                responseDto.setStatus(HttpStatus.NO_CONTENT.value());
                responseDto.setMessage("Lỗi đề tài !");
                responseDto.setObject(null);
            }
        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto update(NhomDTO nhomDTO) {
        ResponseDto responseDto = new ResponseDto();
        User user = contextService.getCurrentUser();

        Optional<Nhom> nhom = nhomRepository.findById(nhomDTO.getId());
        if (nhom.isPresent()) {
            Nhom nhom1 = toNhom.getDestination(nhom.get(), nhomDTO);
            Nhom result = nhomRepository.save(nhom1);
            NhomDTO nhomDTO1 = toNhomDto.getDestination(result);
            responseDto.setObject(nhomDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.BAD_REQUEST.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<Nhom> nhom = nhomRepository.findById(id);
        if (nhom.isPresent()) {
            nhomRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.BAD_REQUEST.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }
}
