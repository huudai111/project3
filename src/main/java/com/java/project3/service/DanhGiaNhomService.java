package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.*;
import com.java.project3.dto.DanhGiaNhomDTO;
import com.java.project3.dto.DeTaiDoAnDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.DanhGiaNhomRepository;
import com.java.project3.repository.NhomRepository;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class DanhGiaNhomService {
    @Autowired
    DanhGiaNhomRepository danhGiaNhomRepository;
    @Autowired
    NhomRepository nhomRepository;
    @Autowired
    GenIdService genIdService;

    JMapper<DanhGiaNHom, DanhGiaNhomDTO> toDanhGiaNHom;
    JMapper<DanhGiaNhomDTO, DanhGiaNHom> toDanhGiaNHomDto;

    public DanhGiaNhomService() {
        this.toDanhGiaNHom = new JMapper<>(DanhGiaNHom.class, DanhGiaNhomDTO.class);
        this.toDanhGiaNHomDto = new JMapper<>(DanhGiaNhomDTO.class, DanhGiaNHom.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DanhGiaNHom> danhGiaNHom = danhGiaNhomRepository.findById(id);
        if (danhGiaNHom.isPresent()) {
            DanhGiaNhomDTO danhGiaNhomDTO = toDanhGiaNHomDto.getDestination(danhGiaNHom.get());
            responseDto.setObject(danhGiaNhomDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi nhóm sinh viên !");
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<DanhGiaNHom> danhGiaNHoms = danhGiaNhomRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<DanhGiaNhomDTO> danhGiaNhomDTOS = new ArrayList<>();
        for (var danhGiaNHom : danhGiaNHoms) {
            DanhGiaNhomDTO danhGiaNhomDTO = toDanhGiaNHomDto.getDestination(danhGiaNHom);
            danhGiaNhomDTOS.add(danhGiaNhomDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(danhGiaNHoms.getTotalPages(), danhGiaNHoms.getNumber(), danhGiaNHoms.getTotalElements(), danhGiaNhomDTOS));

        return responseDto;
    }

    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<DanhGiaNHom> danhGiaNHom = danhGiaNhomRepository.findById(id);
        if (danhGiaNHom.isPresent()){
            danhGiaNhomRepository.delete(danhGiaNHom.get());
            responseDto.setObject(danhGiaNHom.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto create(DanhGiaNhomDTO danhGiaNhomDTO) {
        ResponseDto responseDto = new ResponseDto();
        Optional<Nhom> nhom = nhomRepository.findById(danhGiaNhomDTO.getNhomId());
        if (nhom.isPresent()) {
            DanhGiaNHom danhGiaNHom = toDanhGiaNHom.getDestination(danhGiaNhomDTO);
            danhGiaNHom.setIsDeleted(false);
            danhGiaNHom.setId(genIdService.nextId());
            DanhGiaNHom result = danhGiaNhomRepository.save(danhGiaNHom);
            responseDto.setObject(result);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi đề tài !");
            responseDto.setObject(null);
        }

        return responseDto;
    }

    public ResponseDto update(DanhGiaNhomDTO danhGiaNhomDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<DanhGiaNHom> danhGiaNHom = danhGiaNhomRepository.findById(danhGiaNhomDTO.getId());
        if (danhGiaNHom.isPresent()) {
            DanhGiaNHom danhGiaNHom1 = toDanhGiaNHom.getDestination(danhGiaNHom.get(), danhGiaNhomDTO);
            DanhGiaNHom result = danhGiaNhomRepository.save(danhGiaNHom1);
            DanhGiaNhomDTO danhGiaNhomDTO1 = toDanhGiaNHomDto.getDestination(result);
            responseDto.setObject(danhGiaNhomDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DanhGiaNHom> danhGiaNHom = danhGiaNhomRepository.findById(id);
        if (danhGiaNHom.isPresent()) {
            danhGiaNhomRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto listDanhGia(Integer pageIndex, Integer pageSize, Long nhomId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        searchReqDto.setQuery("N-nhomId=\"" + nhomId + "\"");
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }

    public ResponseDto LocList(Integer pageIndex, Integer pageSize, Long nhomId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        searchReqDto.setQuery("N-nhomId=\"" + nhomId + "\"");
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }
}