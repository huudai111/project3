package com.java.project3.service;

import com.java.project3.domain.Resource;
import com.java.project3.repository.ResourceRepository;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.FileStorageException;
import com.java.project3.utils.StringUtil;
import liquibase.util.file.FilenameUtils;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Service
public class ResourceService {
    @Autowired
    GenIdService genIdService;
    @Autowired
    ResourceRepository resourceRepository;

    public void storeFileGiaiPhap(Long targetId, MultipartFile[] files) {
        if (files != null) {
            for (var item : files) {
                if (!item.getOriginalFilename().equals("")) {
                    String link = storeMultipartFile(item);
                    Resource resource = new Resource();
                    resource.setContentType(item.getContentType());
                    resource.setFileName(item.getOriginalFilename());
                    resource.setLink(link);
                    resource.setTargetId(targetId);
                    resourceRepository.save(resource);
                }
            }
        }
    }

    private Path fileStorageLocation;

    public String storeMultipartFile(MultipartFile file) {
        // Normalize file name
        String originalFilename = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExt = FilenameUtils.getExtension(originalFilename);
        String fileName = FilenameUtils.getBaseName(originalFilename);

        long fileId = genIdService.nextId();
        fileName = StringUtil.covertStringToUrl(fileName);
        fileName = fileName + "-" + fileId + "." + fileExt;

        System.out.println(fileName);
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            System.out.println(targetLocation);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
}
