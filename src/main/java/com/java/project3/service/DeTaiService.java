package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.DeTaiDoAn;
import com.java.project3.domain.DoAn;
import com.java.project3.domain.GiaoVien;
import com.java.project3.domain.Khoa;
import com.java.project3.dto.DeTaiDoAnDTO;
import com.java.project3.dto.GiaoVienDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.DeTaiDoAnRepository;
import com.java.project3.repository.DoAnRepository;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class DeTaiService {
    @Autowired
    DeTaiDoAnRepository deTaiDoAnRepository;
    @Autowired
    DoAnRepository doAnRepository;

    JMapper<DeTaiDoAn, DeTaiDoAnDTO> toDeTaiDoAn;
    JMapper<DeTaiDoAnDTO, DeTaiDoAn> toDeTaiDoAnDto;

    public DeTaiService() {
        this.toDeTaiDoAn = new JMapper<>(DeTaiDoAn.class, DeTaiDoAnDTO.class);
        this.toDeTaiDoAnDto = new JMapper<>(DeTaiDoAnDTO.class, DeTaiDoAn.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DeTaiDoAn> deTaiDoAn = deTaiDoAnRepository.findById(id);
        if (deTaiDoAn.isPresent()) {
            DeTaiDoAnDTO deTaiDoAnDTO = toDeTaiDoAnDto.getDestination(deTaiDoAn.get());
            responseDto.setObject(deTaiDoAnDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi nhóm sinh viên !");
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<DeTaiDoAn> deTaiDoAns = deTaiDoAnRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<DeTaiDoAnDTO> deTaiDoAnDTOS = new ArrayList<>();
        for (var deTaiDoAn : deTaiDoAns) {
            DeTaiDoAnDTO deTaiDoAnDTO = toDeTaiDoAnDto.getDestination(deTaiDoAn);
            deTaiDoAnDTOS.add(deTaiDoAnDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(deTaiDoAns.getTotalPages(), deTaiDoAns.getNumber(), deTaiDoAns.getTotalElements(), deTaiDoAnDTOS));

        return responseDto;
    }

    public ResponseDto create(DeTaiDoAnDTO deTaiDoAnDTO) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(deTaiDoAnDTO.getId());
        if (doAn.isPresent()) {
            DeTaiDoAn deTaiDoAn = toDeTaiDoAn.getDestination(deTaiDoAnDTO);
            deTaiDoAn.setIsDeleted(false);
            DeTaiDoAn result = deTaiDoAnRepository.save(deTaiDoAn);
            responseDto.setObject(result);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi đề tài !");
            responseDto.setObject(null);
        }

        return responseDto;
    }
    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<DeTaiDoAn> deTaiDoAn = deTaiDoAnRepository.findById(id);
        if (deTaiDoAn.isPresent()){
            deTaiDoAnRepository.delete(deTaiDoAn.get());
            responseDto.setObject(deTaiDoAn.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto update(DeTaiDoAnDTO deTaiDoAnDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<DeTaiDoAn> deTaiDoAn = deTaiDoAnRepository.findById(deTaiDoAnDTO.getId());
        if (deTaiDoAn.isPresent()) {
            DeTaiDoAn deTaiDoAn1 = toDeTaiDoAn.getDestination(deTaiDoAn.get(), deTaiDoAnDTO);
            DeTaiDoAn result = deTaiDoAnRepository.save(deTaiDoAn1);
            DeTaiDoAnDTO deTaiDoAnDTO1 = toDeTaiDoAnDto.getDestination(result);
            responseDto.setObject(deTaiDoAnDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DeTaiDoAn> deTaiDoAn = deTaiDoAnRepository.findById(id);
        if (deTaiDoAn.isPresent()) {
            deTaiDoAnRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto listGiaoVien(Integer pageIndex, Integer pageSize, Long doAnId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        searchReqDto.setQuery("N-doAnId=\"" + doAnId + "\"");
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }
}