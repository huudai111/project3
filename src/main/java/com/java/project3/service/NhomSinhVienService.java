package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.DeTaiDoAn;
import com.java.project3.domain.DoAn;
import com.java.project3.domain.Nhom;
import com.java.project3.domain.NhomSinhVien;
import com.java.project3.dto.NhomSinhVienDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.DeTaiDoAnRepository;
import com.java.project3.repository.DoAnRepository;
import com.java.project3.repository.NhomRepository;
import com.java.project3.repository.NhomSinhVienRepository;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class NhomSinhVienService {
    @Autowired
    NhomSinhVienRepository nhomSinhVienRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    DeTaiDoAnRepository deTaiDoAnRepository;
    @Autowired
    NhomRepository nhomRepository;
    @Autowired
    GenIdService genIdService;

    JMapper<NhomSinhVien, NhomSinhVienDTO> toNhomSinhVien;
    JMapper<NhomSinhVienDTO, NhomSinhVien> toNhomSinhVienDto;

    public NhomSinhVienService() {
        this.toNhomSinhVien = new JMapper<>(NhomSinhVien.class, NhomSinhVienDTO.class);
        this.toNhomSinhVienDto = new JMapper<>(NhomSinhVienDTO.class, NhomSinhVien.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<NhomSinhVien> nhomSinhVien = nhomSinhVienRepository.findById(id);
        if (nhomSinhVien.isPresent()) {
            NhomSinhVienDTO nhomSinhVienDTO = toNhomSinhVienDto.getDestination(nhomSinhVien.get());
            Nhom nhom = nhomRepository.findByDoAnId(nhomSinhVien.get().getDoAnId());
            Long dem = nhomSinhVienRepository.findByDoAnId(nhomSinhVienDTO.getDoAnId()).stream().count();
            Optional<DoAn> doAn = doAnRepository.findById(nhomSinhVienDTO.getDoAnId());
            if (doAn.isPresent()){
                nhomSinhVienDTO.setDoAn(doAn.get().getName());
            }
            nhomSinhVienDTO.setDem(dem);
            nhomSinhVienDTO.setName(nhom.getName());
            responseDto.setObject(nhomSinhVienDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi nhóm sinh viên !");
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<NhomSinhVien> nhomSinhViens = nhomSinhVienRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<NhomSinhVienDTO> nhomSinhVienDTOS = new ArrayList<>();
        for (var nhomSinhVien : nhomSinhViens) {
            NhomSinhVienDTO nhomSinhVienDTO = toNhomSinhVienDto.getDestination(nhomSinhVien);
            Nhom nhom = nhomRepository.findByDoAnId(nhomSinhVien.getDoAnId());
            Long dem = nhomSinhVienRepository.findByDoAnId(nhomSinhVienDTO.getDoAnId()).stream().count();
            Optional<DoAn> doAn = doAnRepository.findById(nhomSinhVienDTO.getDoAnId());
            if (doAn.isPresent()){
                nhomSinhVienDTO.setDoAn(doAn.get().getName());
            }
            nhomSinhVienDTO.setDem(dem);
            nhomSinhVienDTO.setName(nhom.getName());
            nhomSinhVienDTOS.add(nhomSinhVienDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(nhomSinhViens.getTotalPages(), nhomSinhViens.getNumber(), nhomSinhViens.getTotalElements(), nhomSinhVienDTOS));

        return responseDto;
    }

    public ResponseDto create(NhomSinhVienDTO nhomSinhVienDTO) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(nhomSinhVienDTO.getDoAnId());
        if (doAn.isPresent()) {
            Nhom nhom = new Nhom();
            nhom.setName(nhomSinhVienDTO.getName());
            nhom.setIsDeleted(false);
            nhom.setId(genIdService.nextId());
            nhom.setDoAnId(nhomSinhVienDTO.getDoAnId());
            nhomRepository.save(nhom);

            List<Long> id = new ArrayList<>();
            id.add(nhomSinhVienDTO.getThanhVienId1());
            id.add(nhomSinhVienDTO.getThanhVienId2());
            id.add(nhomSinhVienDTO.getThanhVienId3());

            for (var i : id) {
                NhomSinhVien nhomSinhVien = new NhomSinhVien();
                nhomSinhVien.setSinhVienId(i);
                nhomSinhVien.setIsDeleted(false);
                nhomSinhVien.setTruongNhom(false);
                nhomSinhVien.setId(genIdService.nextId());
                nhomSinhVien.setDoAnId(nhomSinhVienDTO.getDoAnId());
                NhomSinhVien result = nhomSinhVienRepository.save(nhomSinhVien);
                responseDto.setObject(result);
            }
            NhomSinhVien nhomSinhVien = toNhomSinhVien.getDestination(nhomSinhVienDTO);
            nhomSinhVien.setSinhVienId(nhomSinhVien.getSinhVienId());
            nhomSinhVien.setIsDeleted(false);
            nhomSinhVien.setTruongNhom(true);
            nhomSinhVien.setId(genIdService.nextId());
            NhomSinhVien result = nhomSinhVienRepository.save(nhomSinhVien);
            responseDto.setObject(result);


        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto update(NhomSinhVienDTO nhomSinhVienDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<NhomSinhVien> nhomSinhVien = nhomSinhVienRepository.findById(nhomSinhVienDTO.getId());
        if (nhomSinhVien.isPresent()) {
            NhomSinhVien nhomSinhVien1 = toNhomSinhVien.getDestination(nhomSinhVien.get(), nhomSinhVienDTO);
            NhomSinhVien result = nhomSinhVienRepository.save(nhomSinhVien1);
            NhomSinhVienDTO nhomSinhVienDTO1 = toNhomSinhVienDto.getDestination(result);
            responseDto.setObject(nhomSinhVienDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<NhomSinhVien> nhomSinhVien = nhomSinhVienRepository.findById(id);
        if (nhomSinhVien.isPresent()) {
            nhomSinhVienRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto listNhomSinhVien(Integer pageIndex, Integer pageSize, Long nhomId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        if (nhomId == null) {
            searchReqDto.setQuery(" ");

        } else {
            searchReqDto.setQuery("N-nhomId=\"" + nhomId + "\"");
        }
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }

    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<NhomSinhVien> nhomSinhVien = nhomSinhVienRepository.findById(id);
        if (nhomSinhVien.isPresent()){
            nhomSinhVienRepository.delete(nhomSinhVien.get());
            responseDto.setObject(nhomSinhVien.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }
}
