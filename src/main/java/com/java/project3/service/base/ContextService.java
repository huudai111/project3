package com.java.project3.service.base;

import com.java.project3.domain.User;
import com.java.project3.dto.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class ContextService {
    @Autowired
    HttpServletRequest request;


    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            if (authentication.isAuthenticated()) {
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                Object principal = auth.getPrincipal();
                if (principal instanceof User) {
                    return (User) principal;
                } else if (principal instanceof RequestContext) {
                    RequestContext requestContext = (RequestContext) auth.getPrincipal();
                    return requestContext.getUser();
                } else {
                    return null;
                }
            }
        } else if (authentication instanceof RememberMeAuthenticationToken) {
            if (authentication.isAuthenticated()) {
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                Object principal = auth.getPrincipal();
                if (principal instanceof User) {
                    return (User) principal;
                } else if (principal instanceof RequestContext) {
                    RequestContext requestContext = (RequestContext) auth.getPrincipal();
                    return requestContext.getUser();
                } else {
                    return null;
                }
            }
        } else if (authentication instanceof AnonymousAuthenticationToken) {
            AnonymousAuthenticationToken anonymousAuthenticationToken = (AnonymousAuthenticationToken) authentication;
            return null;
        }
        return null;
    }


//    public ROLE_NAME getRoleName() {
//        if (this.hasROLE_ADMIN()) {
//            return ROLE_NAME.ROLE_ADMIN;
//        } else if (this.hasROLE_ENTERPRISE_QUAN_TRI()) {
//            return ROLE_NAME.ROLE_ENTERPRISE_QUAN_TRI;
//        } else if (this.hasROLE_ENTERPRISE_QUAN_LY()) {
//            return ROLE_NAME.ROLE_ENTERPRISE_QUAN_LY;
//        } else if (this.hasROLE_ENTERPRISE_QUAN_LY_THUC_TAP()) {
//            return ROLE_NAME.ROLE_ENTERPRISE_QUAN_LY_THUC_TAP;
//        } else if (this.hasROLE_UNIVERSITY_QUAN_TRI()) {
//            return ROLE_NAME.ROLE_UNIVERSITY_QUAN_TRI;
//        } else if (this.hasROLE_UNIVERSITY_QUAN_LY()) {
//            return ROLE_NAME.ROLE_UNIVERSITY_QUAN_LY;
//        } else if (this.hasROLE_UNIVERSITY_SINH_VIEN()) {
//            return ROLE_NAME.ROLE_UNIVERSITY_SINH_VIEN;
//        } else if (this.hasROLE_UNIVERSITY_GIANG_VIEN()) {
//            return ROLE_NAME.ROLE_UNIVERSITY_GIANG_VIEN;
//        } else if (this.hasROLE_PERSONAL()) {
//            return ROLE_NAME.ROLE_PERSONAL;
//        } else {
//            return ROLE_NAME.ROLE_PUBLIC;
//        }
//    }


//    public boolean hasROLE_ADMIN() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_ADMIN.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_ENTERPRISE_QUAN_TRI() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_ENTERPRISE_QUAN_TRI.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_ENTERPRISE_QUAN_LY() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_ENTERPRISE_QUAN_LY.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_ENTERPRISE_QUAN_LY_THUC_TAP() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_ENTERPRISE_QUAN_LY_THUC_TAP.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_UNIVERSITY_QUAN_TRI() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_QUAN_TRI.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_UNIVERSITY_QUAN_LY() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_QUAN_LY.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//
//    }
//
//    public boolean hasROLE_UNIVERSITY_SINH_VIEN() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_SINH_VIEN.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//
//    }
//
//    public boolean hasROLE_UNIVERSITY_GIANG_VIEN() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_GIANG_VIEN.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//
//    }
//
//    public List<Organization> findAllOrgByTypeAndCurrentUser(TARGET_TYPE target_type){
//        List<Organization> list = new ArrayList<>();
//        User user = getCurrentUser();
//        if(user != null){
//            if(user.getAllOrg().size() > 0){
//                for (Organization organization : user.getAllOrg()){
//                    if(organization.getType() != null && organization.getType().shortValue() == target_type.getValue().shortValue()){
//                        list.add(organization);
//                    }
//                }
//            }
//        }
//        return list;
//    }
//
//    public boolean hasROLE_PERSONAL() {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_PERSONAL.name())).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//
//    }
//
//    // region new
//    public List<ROLE_NAME> getAllRoleNameInOrg(Long orgId) {
//        User user = getCurrentUser();
//        List<ROLE_NAME> roleNames = new ArrayList<>();
//        List<Role> roles = user.getAllRole().stream().filter(p->p.getOrgId() != null && p.getOrgId().equals(orgId)).collect(Collectors.toList());
//        for (var role : roles){
//            ROLE_NAME roleName = ROLE_NAME.findByRoleNameString(role.getName());
//            if(roleName != null){
//                roleNames.add(roleName);
//            }
//        }
//        return roleNames;
//    }
//
//    public boolean hasROLE_ENTERPRISE_QUAN_TRI_IN_ORG(Long orgId) {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_ENTERPRISE_QUAN_TRI.name()) && x.getOrgId().equals(orgId)).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_ENTERPRISE_QUAN_LY_IN_ORG(Long orgId) {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_ENTERPRISE_QUAN_LY.name()) && x.getOrgId().equals(orgId)).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_ENTERPRISE_QUAN_LY_THUC_TAP_IN_ORG(Long orgId) {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_ENTERPRISE_QUAN_LY_THUC_TAP.name()) && x.getOrgId().equals(orgId)).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_UNIVERSITY_QUAN_TRI_IN_ORG(Long orgId) {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_QUAN_TRI.name()) && x.getOrgId().equals(orgId)).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_UNIVERSITY_QUAN_LY_IN_ORG(Long orgId) {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_QUAN_LY.name()) && x.getOrgId().equals(orgId)).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_UNIVERSITY_SINH_VIEN_IN_ORG(Long orgId) {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_SINH_VIEN.name()) && x.getOrgId().equals(orgId)).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public boolean hasROLE_UNIVERSITY_GIANG_VIEN_IN_ORG(Long orgId) {
//        User user = this.getCurrentUser();
//        if (user != null) {
//            Optional<Role> role = user.getRoles().stream().filter(x -> x.getName().equals(ROLE_NAME.ROLE_UNIVERSITY_GIANG_VIEN.name()) && x.getOrgId().equals(orgId)).findAny();
//            if (role.isPresent()) {
//                return true;
//            }
//        }
//        return false;
//    }
    // endregion
}
