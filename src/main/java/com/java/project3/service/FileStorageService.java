package com.java.project3.service;

import com.java.project3.configuration.FileStorageException;
import com.java.project3.configuration.MyFileNotFoundException;
import com.java.project3.dto.UploadFileResponse;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.StringUtil;
import liquibase.util.file.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class FileStorageService {
    private Path fileStorageLocation;
    private Path dirCensorship;
    private Path dirThucTap;
    private Path dirTemplate;
    private Path dirTemp;
    @Value("${external.port}")
    String port;
    @Autowired
    GenIdService genIdService;

    public FileStorageService(@Value("${file.upload_dir}") String uploadDir) {
        this.fileStorageLocation = Paths.get(uploadDir)
                .toAbsolutePath().normalize();
        this.dirCensorship = Paths.get(fileStorageLocation + "/censorship");
        this.dirThucTap = Paths.get(fileStorageLocation + "/thuctap");
        this.dirTemplate = Paths.get(fileStorageLocation + "/template");
        this.dirTemp = Paths.get(fileStorageLocation + "/temp");
        try {
            Files.createDirectories(this.fileStorageLocation);
            if (!Files.exists(dirCensorship)) {
                Files.createDirectories(this.dirCensorship);
            }
            if (!Files.exists(dirThucTap)) {
                Files.createDirectories(this.dirThucTap);
            }
            if (!Files.exists(dirTemplate)) {
                Files.createDirectories(this.dirTemplate);
            }
            if (!Files.exists(dirTemp)) {
                Files.createDirectories(this.dirTemp);
            }
        } catch (Exception ex) {
            System.out.println("Could not create the directory where the uploaded files will be stored.");
        }
    }

    public void createFolder(String folderName) throws IOException {
        if (!Files.exists(Paths.get(fileStorageLocation + "/temp/" + folderName))) {
            Files.createDirectories(Paths.get(fileStorageLocation + "/temp/" + folderName));
        }
    }

    public Path getTempPath() {
        return this.dirTemp;
    }

    public void handlerCopyFileToFolderGiaiPhap(String link, String folder) throws IOException {
        Path folderIn = Paths.get(dirTemp + "/" + folder, link);
        Path filePath = this.fileStorageLocation.resolve(link).normalize();
        if (Files.exists(filePath)) {
            Files.copy(filePath, folderIn, StandardCopyOption.REPLACE_EXISTING);
        }
    }


    public void zip(String fileZipName, String folderName) throws IOException {
        Path zipFile = Paths.get(fileStorageLocation + "/" + fileZipName);
        Path sourceDirPath = Paths.get(dirTemp + "/" + folderName);
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(zipFile));
             Stream<Path> paths = Files.walk(sourceDirPath)) {
            paths
                    .filter(path -> !Files.isDirectory(path))
                    .forEach(path -> {
                        ZipEntry zipEntry = new ZipEntry(sourceDirPath.relativize(path).toString());
                        try {
                            zipOutputStream.putNextEntry(zipEntry);
                            Files.copy(path, zipOutputStream);
                            zipOutputStream.closeEntry();
                        } catch (IOException e) {
                            System.err.println(e);
                        }
                    });
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String storeMultipartFile(MultipartFile file) {
        // Normalize file name
        String originalFilename = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExt = FilenameUtils.getExtension(originalFilename);
        String fileName = FilenameUtils.getBaseName(originalFilename);

        long fileId = genIdService.nextId();
        fileName = StringUtil.covertStringToUrl(fileName);
        fileName = fileName + "-" + fileId + "." + fileExt;

        System.out.println(fileName);
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            System.out.println(targetLocation);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    // region censorship
    public String storeCensorshipFile(MultipartFile file) {
        String originalFilename = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExt = FilenameUtils.getExtension(originalFilename);
        String fileName = FilenameUtils.getBaseName(originalFilename);

        long fileId = genIdService.nextId();
        fileName = StringUtil.covertStringToUrl(fileName);
        fileName = fileName + "-" + fileId + "." + fileExt;

        System.out.println(fileName);
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.dirCensorship.resolve(fileName);
            System.out.println(targetLocation);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public String storeThucTapFile(MultipartFile file, Long universityId) throws IOException {
        Path path = Paths.get(dirThucTap + "/" + universityId).toAbsolutePath().normalize();
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }

        String originalFilename = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExt = FilenameUtils.getExtension(originalFilename);
        String fileName = FilenameUtils.getBaseName(originalFilename);

        long fileId = genIdService.nextId();
        fileName = StringUtil.covertStringToUrl(fileName);
        fileName = fileName + "-" + fileId + "." + fileExt;

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = path.resolve(fileName);

            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return "thuctap/" + universityId + "/" + fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public boolean deleteThucTapFile(String path) throws IOException {
        Path path1 = Paths.get(path).toAbsolutePath().normalize();
        if (Files.exists(path1)) {
            Files.delete(path1);
            return true;
        } else {
            return false;
        }
    }

    public Resource loadCensorshipFileAsResource(String fileName) {
        try {
            Path filePath = this.dirCensorship.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }
    //enregion

    public Path getBaseFilePath() {
        return fileStorageLocation.toAbsolutePath();
    }


    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public String storeTemplateFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        System.out.println(fileName);
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.dirTemplate.resolve(fileName);
            System.out.println(targetLocation);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadTemplateFileAsResource(String fileName) {
        try {
            Path filePath = this.dirTemplate.resolve(fileName).normalize();
            System.out.println("filePath " + filePath);
            System.out.println("filePath toUri " + filePath.toUri());
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                System.out.println("File not found " + fileName);
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            System.out.println("File not found " + fileName);
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public String getFilePath(String fileName) {
        Path targetLocation = this.getBaseFilePath();
        return targetLocation.toAbsolutePath() + "/" + fileName;
    }

    public UploadFileResponse responseFileData(String fileName) {
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .port(port)
                .path("/file/view/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse(1, fileName, fileDownloadUri,
                "", 0);
    }
}
