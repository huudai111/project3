package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.Khoa;
import com.java.project3.domain.SinhVien;
import com.java.project3.domain.User;
import com.java.project3.dto.CreateSinhVienDTO;
import com.java.project3.dto.SinhVienDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.SinhVienRepository;
import com.java.project3.service.base.ContextService;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class SinhVienService {
    @Autowired
    SinhVienRepository sinhVienRepository;
    @Autowired
    ContextService contextService;
    @Autowired
    GenIdService genIdService;

    JMapper<SinhVien, SinhVienDTO> toSinhVien;
    JMapper<SinhVienDTO, SinhVien> toSinhVienDto;
    JMapper<SinhVien, CreateSinhVienDTO> toCreateSinhVienDto;

    public SinhVienService() {
        this.toSinhVien = new JMapper<>(SinhVien.class, SinhVienDTO.class);
        this.toSinhVienDto = new JMapper<>(SinhVienDTO.class, SinhVien.class);
        this.toCreateSinhVienDto = new JMapper<>(SinhVien.class, CreateSinhVienDTO.class);

    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<SinhVien> sinhVien = sinhVienRepository.findById(id);
        if (sinhVien.isPresent()) {
            SinhVienDTO sinhVienDTO = toSinhVienDto.getDestination(sinhVien.get());
            responseDto.setObject(sinhVienDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<SinhVien> sinhViens = sinhVienRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<SinhVienDTO> sinhVienDTOS = new ArrayList<>();
        for (var sinhVien : sinhViens) {
            SinhVienDTO sinhVienDTO = toSinhVienDto.getDestination(sinhVien);
            sinhVienDTOS.add(sinhVienDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(sinhViens.getTotalPages(), sinhViens.getNumber(), sinhViens.getTotalElements(), sinhVienDTOS));

        return responseDto;
    }

    public ResponseDto listSinhVien(Integer pageIndex, Integer pageSize, Long khoaId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        if (khoaId == null) {
            searchReqDto.setQuery(" ");

        } else {
            searchReqDto.setQuery("N-khoaId=\"" + khoaId + "\"");

        }
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }

    public ResponseDto create(CreateSinhVienDTO createSinhVienDTO) {
        ResponseDto responseDto = new ResponseDto();
        User user = contextService.getCurrentUser();
        Optional<SinhVien> sinhVien = Optional.ofNullable(sinhVienRepository.findByMaSv(createSinhVienDTO.getMaSv()));
        if (sinhVien.isPresent()) {
            responseDto.setStatus(400);
            responseDto.setObject(null);
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
        } else {
            createSinhVienDTO.setId(genIdService.nextId());
            SinhVien sinhVien1 = toCreateSinhVienDto.getDestination(createSinhVienDTO);
            sinhVien1.setIsDeleted(false);
            var temp = sinhVienRepository.save(sinhVien1);
            responseDto.setObject(temp);
        }
        return responseDto;
    }

    public ResponseDto update(SinhVienDTO sinhVienDTO) {
        ResponseDto responseDto = new ResponseDto();
        User user = contextService.getCurrentUser();

        Optional<SinhVien> sinhVien = sinhVienRepository.findById(sinhVienDTO.getId());
        if (sinhVien.isPresent()) {
            SinhVien sinhVien1 = toSinhVien.getDestination(sinhVien.get(), sinhVienDTO);
            SinhVien result = sinhVienRepository.save(sinhVien1);
            SinhVienDTO sinhVienDTO1 = toSinhVienDto.getDestination(result);
            responseDto.setObject(sinhVienDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.BAD_REQUEST.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<SinhVien> sinhVien = sinhVienRepository.findById(id);
        if (sinhVien.isPresent()) {
            sinhVienRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.BAD_REQUEST.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto delete(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<SinhVien> sinhVien = sinhVienRepository.findById(id);
        if (sinhVien.isPresent()) {
            sinhVienRepository.delete(sinhVien.get());
            responseDto.setObject(sinhVien.get());
        } else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public List<SinhVien> listAll() {
        return sinhVienRepository.findAll(Sort.by("email").ascending());
    }


    public ResponseDto searchSinhVien(Integer pageIndex, Integer pageSize, String search) {
        ResponseDto responseDto = new ResponseDto();
        if (pageIndex == null){
            pageIndex = 0;
        }
        if (pageSize == null){
            pageSize = 5;
        }
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize);
        SinhVienDTO sinhVienDTO = new SinhVienDTO();
        if (search == null) {
            search = "";
        }
        Page<SinhVien> page = sinhVienRepository.searchSinhVien(search, pageRequest);
        for (var sinhvien : page) {
            List<SinhVienDTO> sinhVienDTOList = new ArrayList<>();
            sinhVienDTO = toSinhVienDto.getDestination(sinhvien);
            sinhVienDTOList.add(sinhVienDTO);
            responseDto.setObject(prepareResponseForSearch(page.getTotalPages(), page.getNumber(), page.getTotalElements(), sinhVienDTOList));
        }
        return responseDto;
    }
}
