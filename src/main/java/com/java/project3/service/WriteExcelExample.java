package com.java.project3.service;

import com.java.project3.domain.SinhVien;
import com.java.project3.utils.StringUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class WriteExcelExample {
    public static final int COLUMN_NAME         = 0;
    public static final int COLUMN_TEN_LOP      = 1;
    public static final int COLUMN_NGAY_SINH      = 2;
    public static final int COLUMN_DIA_CHI   = 3;
    public static final int COLUMN_SDT      = 4;
    public static final int COLUMN_EMAIL      = 5;
    public static final int COLUMN_MA_SV      = 6;

    private static CellStyle cellStyleFormatNumber = null;

//    public static void main(String[] args) throws IOException {
//        final List<SinhVien> sinhViens = getSinhVien();
//        final String excelFilePath = "C:/demo/books.xlsx";
//        writeExcel(sinhViens, excelFilePath);
//    }

    public static void writeExcel(List<SinhVien> sinhViens, String excelFilePath) throws IOException {
        // Create Workbook
        Workbook workbook = getWorkbook(excelFilePath);

        // Create sheet
        Sheet sheet = workbook.createSheet("Books"); // Create sheet with sheet name

        int rowIndex = 0;

        // Write header
        writeHeader(sheet, rowIndex);

        // Write data
        rowIndex++;
        for (SinhVien sinhVien : sinhViens) {
            // Create row
            Row row = sheet.createRow(rowIndex);
            // Write data on row
            writeBook(sinhVien, row);
            rowIndex++;
        }
//
//        // Write footer
//        writeFooter(sheet, rowIndex);

        // Auto resize column witdth
        int numberOfColumn = sheet.getRow(0).getPhysicalNumberOfCells();
        autosizeColumn(sheet, numberOfColumn);

        // Create file excel
        createOutputFile(workbook, excelFilePath);
        System.out.println("Done!!!");
    }

    // Create dummy data
    private static List<SinhVien> getSinhVien() {
        List<SinhVien> listSinhVien = new ArrayList<>();
        SinhVien sinhVien;
        for (int i = 1; i <= 6; i++) {
            sinhVien = new SinhVien(i, "SinhVien " + i, i * 2, i * 1000);
            listSinhVien.add(sinhVien);
        }
        return listSinhVien;
    }

    // Create workbook
    private static Workbook getWorkbook(String excelFilePath) throws IOException {
        Workbook workbook = null;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook();
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }

    // Write header with format
    private static void writeHeader(Sheet sheet, int rowIndex) {
        // create CellStyle
        CellStyle cellStyle = createStyleForHeader(sheet);

        // Create row
        Row row = sheet.createRow(rowIndex);

        // Create cells
        Cell cell = row.createCell(COLUMN_NAME);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Tên");

        cell = row.createCell(COLUMN_TEN_LOP);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Tên lớp");

        cell = row.createCell(COLUMN_NGAY_SINH);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Ngày sinh");

        cell = row.createCell(COLUMN_DIA_CHI);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Địa chỉ");

        cell = row.createCell(COLUMN_SDT);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("SĐT");

        cell = row.createCell(COLUMN_EMAIL);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Email");

        cell = row.createCell(COLUMN_MA_SV);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Mã sinh viên");

    }

    // Write data
    private static void writeBook(SinhVien sinhVien, Row row) {
        if (cellStyleFormatNumber == null) {
            // Format number
            short format = (short) BuiltinFormats.getBuiltinFormat("#,##0");
            // DataFormat df = workbook.createDataFormat();
            // short format = df.getFormat("#,##0");

            //Create CellStyle
            Workbook workbook = row.getSheet().getWorkbook();
            cellStyleFormatNumber = workbook.createCellStyle();
            cellStyleFormatNumber.setDataFormat(format);
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String formattedString = String.valueOf(sinhVien.getNgaySinh());

        Cell cell = row.createCell(COLUMN_TEN_LOP);
        cell.setCellValue(sinhVien.getTenLop());

        cell = row.createCell(COLUMN_NGAY_SINH);
        cell.setCellValue(formattedString);

        cell = row.createCell(COLUMN_DIA_CHI);
        cell.setCellValue(sinhVien.getDiaChi());

        cell = row.createCell(COLUMN_SDT);
        cell.setCellValue(sinhVien.getSdt());
        cell.setCellStyle(cellStyleFormatNumber);

        cell = row.createCell(COLUMN_EMAIL);
        cell.setCellValue(sinhVien.getEmail());

        cell = row.createCell(COLUMN_MA_SV);
        cell.setCellValue(sinhVien.getMaSv());

    }

    // Create CellStyle for header
    private static CellStyle createStyleForHeader(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        return cellStyle;
    }

    // Write footer
//    private static void writeFooter(Sheet sheet, int rowIndex) {
//        // Create row
//        Row row = sheet.createRow(rowIndex);
//        Cell cell = row.createCell(COLUMN_INDEX_TOTAL, CellType.FORMULA);
//        cell.setCellFormula("SUM(E2:E6)");
//    }

    // Auto resize column width
    private static void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    // Create output file
    private static void createOutputFile(Workbook workbook, String excelFilePath) throws IOException {
        try (OutputStream os = new FileOutputStream(excelFilePath)) {
            workbook.write(os);
        }
    }

}
