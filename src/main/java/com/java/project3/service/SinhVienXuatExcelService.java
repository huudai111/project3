package com.java.project3.service;

import com.java.project3.domain.SinhVien;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Service
public class SinhVienXuatExcelService {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<SinhVien> listSinhVien;

    public SinhVienXuatExcelService(List<SinhVien> listSinhVien) {
        this.listSinhVien = listSinhVien;
        workbook = new XSSFWorkbook();
    }


    private void vietTieuDe() {
        sheet = workbook.createSheet("sinhVien");
        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        taoCell(row, 0, "Tên", style);
        taoCell(row, 1, "Sđt", style);
        taoCell(row, 2, "Email", style);
        taoCell(row, 3, "Mã sinh viên", style);
        taoCell(row, 4, "Địa chỉ", style);
        taoCell(row, 5, "Ngày sinh", style);
        taoCell(row, 6, "Giới tính", style);

    }

    private void taoCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Long) {
            cell.setCellValue((Long) value);
        } else if (value instanceof LocalDate) {
            cell.setCellValue(String.valueOf((LocalDate) value));
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void chenDuLieu() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (SinhVien sinhVien : listSinhVien) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            taoCell(row, columnCount++, sinhVien.getName(), style);
            taoCell(row, columnCount++, sinhVien.getSdt(), style);
            taoCell(row, columnCount++, sinhVien.getEmail(), style);
            taoCell(row, columnCount++, sinhVien.getMaSv(), style);
            taoCell(row, columnCount++, sinhVien.getDiaChi(), style);
            taoCell(row, columnCount++, sinhVien.getNgaySinh(), style);
            if (sinhVien.getGioiTinh() == 0) {
                taoCell(row, columnCount++, "Nam", style);
            } else {
                taoCell(row, columnCount++, "Nữ ", style);

            }
        }
    }

    public void xuat(HttpServletResponse response) throws IOException {
        vietTieuDe();
        chenDuLieu();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}

