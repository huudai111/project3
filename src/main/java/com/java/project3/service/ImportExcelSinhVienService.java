package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.annotations.JMap;
import com.java.project3.domain.Khoa;
import com.java.project3.domain.SinhVien;
import com.java.project3.dto.CreateSinhVienDTO;
import com.java.project3.dto.ReadExcelImportStudentDTO;
import com.java.project3.dto.SinhVienDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.repository.KhoaRepository;
import com.java.project3.repository.SinhVienRepository;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.Regex;
import com.java.project3.utils.excel.ParseExcel;
import lombok.var;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@Service
public class ImportExcelSinhVienService {
    @Autowired
    SinhVienRepository sinhVienRepository;
    @Autowired
    KhoaRepository khoaRepository;
    @Autowired
    SinhVienService sinhVienService;
    @Autowired
    GenIdService genIdService;

    JMapper<CreateSinhVienDTO, SinhVien> toSinhVienDto;

    public ImportExcelSinhVienService() {
        this.toSinhVienDto = new JMapper<>(CreateSinhVienDTO.class, SinhVien.class);
    }

    public ResponseDto CreateDataFromExcel(MultipartFile multipartFile) throws IOException, MessagingException, ParseException {
        ResponseDto responseDto = new ResponseDto();
        XSSFWorkbook wb = new XSSFWorkbook(multipartFile.getInputStream());
        var allSheet = wb.sheetIterator();
        List<ReadExcelImportStudentDTO> readExcelImportStudentDTOS = new ArrayList<>();
        while (allSheet.hasNext()) {
            DecimalFormat df = new DecimalFormat("###.###");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            Sheet curSheet = allSheet.next();
            var allRowOneSheet = curSheet.iterator();
            int y = 0;
            while (allRowOneSheet.hasNext()) {
                ReadExcelImportStudentDTO readExcelImportStudentDTO = new ReadExcelImportStudentDTO();
                var re = "^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$";

                List<Short> errorCells = new ArrayList<>();
                y++;
                Row curRow = allRowOneSheet.next();

                if (y < 2) {
                    continue;
                }

                //ma sinh vien
                if (curRow.getCell(1) == null || ParseExcel.getStringValueFromCell(curRow, 1).length() <= 0) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setMaSinhVien(" ");
                    errorCells.add((short) 1);
                } else {
                    var a = ParseExcel.getStringValueFromCell(curRow, 1);

                    Optional<SinhVien> sinhViens = Optional.ofNullable(sinhVienRepository.findByMaSv(a));
                    if (sinhViens.isPresent() && sinhViens.get().getMaSv().equals(a)) {
                        readExcelImportStudentDTO.setRowNumber(y);
                        readExcelImportStudentDTO.setMaSinhVien(a);
                        errorCells.add((short) 1);
                    }
                }

                //  ten sinh vien
                if (curRow.getCell(2) == null || ParseExcel.getStringValueFromCell(curRow, 2).length() <= 0) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setName(" ");
                    errorCells.add((short) 2);
                }

                // ngay sinh
                if (curRow.getCell(3) == null) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setBirthDate(" ");
                    errorCells.add((short) 3);
                } else {
                    if (!ParseExcel.getStringValueFromCell(curRow, 3).matches(re)) {
                        if (!ParseExcel.getStringValueFromCell(curRow, 3).matches("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$")) {
                            readExcelImportStudentDTO.setRowNumber(y);
                            readExcelImportStudentDTO.setBirthDate(ParseExcel.getStringValueFromCell(curRow, 3));
                            errorCells.add((short) 3);
                        }
                    }
                }

                // gioi tinh
                if (curRow.getCell(4) == null || ParseExcel.getStringValueFromCell(curRow, 4).length() <= 0) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setGender(" ");
                    errorCells.add((short) 4);
                }

                //sdt
                if (curRow.getCell(5) == null) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setPhone(" ");
                    errorCells.add((short) 5);
                } else if (!Regex.isPhoneFormat("0" + ParseExcel.getStringValueFromCell(curRow, 5))) {
                    if (!Regex.isPhoneFormat(ParseExcel.getStringValueFromCell(curRow, 5))) {
                        readExcelImportStudentDTO.setRowNumber(y);
                        readExcelImportStudentDTO.setPhone(ParseExcel.getStringValueFromCell(curRow, 5));
                        errorCells.add((short) 5);
                    }
                }
                // email

                if (curRow.getCell(6) == null) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setEmail(" ");
                    errorCells.add((short) 6);
                } else if (!Regex.isEmailFormat(ParseExcel.getStringValueFromCell(curRow, 6))) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setEmail(ParseExcel.getStringValueFromCell(curRow, 6));
                    errorCells.add((short) 6);
                }

                // dia chi
                if (curRow.getCell(7) == null || ParseExcel.getStringValueFromCell(curRow, 7).length() <= 0) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setAddress(" ");
                    errorCells.add((short) 7);
                }
                // khoa

                if (curRow.getCell(8) == null || ParseExcel.getStringValueFromCell(curRow, 8).length() <= 0) {
                    readExcelImportStudentDTO.setRowNumber(y);
                    readExcelImportStudentDTO.setTenKhoa(" ");
                    errorCells.add((short) 8);
                }

                if (readExcelImportStudentDTO.getRowNumber() != null) {
                    readExcelImportStudentDTO.setErrorCells(errorCells);
                    readExcelImportStudentDTOS.add(readExcelImportStudentDTO);

                    responseDto.setStatus(HttpStatus.BAD_REQUEST.value());
                    responseDto.setMessage("Import file thất bại !");
                    responseDto.setObject(null);
                } else {

                    SinhVien sinhVien = new SinhVien();
                    sinhVien.setName(ParseExcel.getStringValueFromCell(curRow, 2));
                    sinhVien.setMaSv(ParseExcel.getStringValueFromCell(curRow, 1));
//                    Optional<Khoa> khoa = Optional.ofNullable(khoaRepository.findByName(ParseExcel.getStringValueFromCell(curRow, 8)));
//                    if (khoa.isPresent()) {
                        sinhVien.setTenKhoa(ParseExcel.getStringValueFromCell(curRow, 8));
//                    }
                    sinhVien.setEmail(ParseExcel.getStringValueFromCell(curRow, 6));
                    sinhVien.setSdt("0" + ParseExcel.getStringValueFromCell(curRow, 5));
                    sinhVien.setDiaChi(ParseExcel.getStringValueFromCell(curRow, 7));
                    sinhVien.setIsDeleted(false);
                    if (!ParseExcel.getStringValueFromCell(curRow, 3).matches(re)) {
                        sinhVien.setNgaySinh(Date.valueOf(ParseExcel.getStringValueFromCell(curRow, 3)));

                    }
                    String gioi_tinh = ParseExcel.getStringValueFromCell(curRow, 4).toLowerCase(Locale.ROOT).replace(" ", "");
                    if (gioi_tinh.equals("nam") || gioi_tinh.equals("men")) {
                        sinhVien.setGioiTinh((short) 0);
                    } else {
                        sinhVien.setGioiTinh((short) 1);
                    }
                    CreateSinhVienDTO createSinhVienDTO = toSinhVienDto.getDestination(sinhVien);
//                    sinhVienService.create(createSinhVienDTO);
                    sinhVien.setId(genIdService.nextId());
                    sinhVienRepository.save(sinhVien);
                }

            }
        }
        responseDto.setObject(readExcelImportStudentDTOS);
        return responseDto;
    }
}