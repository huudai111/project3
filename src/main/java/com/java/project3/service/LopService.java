package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.*;
import com.java.project3.dto.*;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.LopRepository;
import com.java.project3.repository.SinhVienRepository;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class LopService {
    @Autowired
    LopRepository lopRepository;
    @Autowired
    SinhVienRepository sinhVienRepository;
    @Autowired
    GenIdService genIdService;

    JMapper<Lop, LopDTO> toLop;
    JMapper<LopDTO, Lop> toLopDto;

    public LopService() {
        this.toLop = new JMapper<>(Lop.class, LopDTO.class);
        this.toLopDto = new JMapper<>(LopDTO.class, Lop.class);
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<Lop> lops = lopRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<LopDTO> lopDTOS = new ArrayList<>();
        for (var lop : lops) {
            LopDTO lopDTO = toLopDto.getDestination(lop);
            lopDTO.setDem(sinhVienRepository.countByLopId(lop.getId()));
            lopDTOS.add(lopDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(lops.getTotalPages(), lops.getNumber(), lops.getTotalElements(), lopDTOS));

        return responseDto;
    }

    public ResponseDto searchLop(Integer pageIndex, Integer pageSize, String search) {
        ResponseDto responseDto = new ResponseDto();
        if (pageIndex == null){
            pageIndex = 0;
        }
        if (pageSize == null){
            pageSize = 5;
        }
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize);
        LopDTO lopDTO = new LopDTO();
        if (search == null) {
            search = "";
        }
        Page<Lop> page = lopRepository.searchLop(search, pageRequest);
        for (var lop : page) {
            List<LopDTO> lopDTOList = new ArrayList<>();
            lopDTO = toLopDto.getDestination(lop);
            lopDTO.setDem(sinhVienRepository.countByLopId(lop.getId()));
            lopDTOList.add(lopDTO);
            responseDto.setObject(prepareResponseForSearch(page.getTotalPages(), page.getNumber(), page.getTotalElements(), lopDTOList));
        }
        return responseDto;
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<Lop> lop = lopRepository.findById(id);
        if (lop.isPresent()) {
            LopDTO lopDTO = toLopDto.getDestination(lop.get());
            responseDto.setObject(lopDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi đồ án !");
            responseDto.setObject(null);
        }
        return responseDto;
    }
    public ResponseDto listLop(Integer pageIndex, Integer pageSize, Long khoaId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        if (khoaId == null){
            searchReqDto.setQuery(" ");

        }else {
            searchReqDto.setQuery("N-khoaId=\"" + khoaId + "\"");

        }
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }

    public ResponseDto create(LopDTO lopDTO) {
        ResponseDto responseDto = new ResponseDto();
        Lop lop = toLop.getDestination(lopDTO);
        lop.setId(genIdService.nextId());
        lop.setIsDeleted(false);
        Lop result = lopRepository.save(lop);
        responseDto.setObject(result);
        return responseDto;
    }

    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<Lop> lop = lopRepository.findById(id);
        if (lop.isPresent()){
            lopRepository.delete(lop.get());
            responseDto.setObject(lop.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto update(LopDTO lopDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<Lop> lop = lopRepository.findById(lopDTO.getId());
        if (lop.isPresent()) {
            Lop lop1 = toLop.getDestination(lop.get(), lopDTO);
            Lop result = lopRepository.save(lop1);
            LopDTO lopDTO1 = toLopDto.getDestination(result);
            responseDto.setObject(lopDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }
}
