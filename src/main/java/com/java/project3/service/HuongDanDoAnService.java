package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.*;
import com.java.project3.dto.HuongDanDoAnDTO;
import com.java.project3.dto.NhomSinhVienDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.*;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class HuongDanDoAnService {
    @Autowired
    HuongDanDoAnRepository huongDanDoAnRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    DeTaiDoAnRepository deTaiDoAnRepository;
    @Autowired
    NhomRepository nhomRepository;
    @Autowired
            GiaoVienRepository giaoVienRepository;

    JMapper<HuongDanDoAn, HuongDanDoAnDTO> toHuongDanDoAn;
    JMapper<HuongDanDoAnDTO, HuongDanDoAn> toHuongDanDoAnDto;

    public HuongDanDoAnService() {
        this.toHuongDanDoAn = new JMapper<>(HuongDanDoAn.class, HuongDanDoAnDTO.class);
        this.toHuongDanDoAnDto = new JMapper<>(HuongDanDoAnDTO.class, HuongDanDoAn.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<HuongDanDoAn> huongDanDoAn = huongDanDoAnRepository.findById(id);
        if (huongDanDoAn.isPresent()) {
            HuongDanDoAnDTO huongDanDoAnDTO = toHuongDanDoAnDto.getDestination(huongDanDoAn.get());
            responseDto.setObject(huongDanDoAnDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi nhóm sinh viên !");
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<HuongDanDoAn> huongDanDoAns = huongDanDoAnRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<HuongDanDoAnDTO> huongDanDoAnDTOS = new ArrayList<>();
        for (var huongDanDoAn : huongDanDoAns) {
            HuongDanDoAnDTO huongDanDoAnDTO = toHuongDanDoAnDto.getDestination(huongDanDoAn);
            huongDanDoAnDTOS.add(huongDanDoAnDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(huongDanDoAns.getTotalPages(), huongDanDoAns.getNumber(), huongDanDoAns.getTotalElements(), huongDanDoAnDTOS));

        return responseDto;
    }

    public ResponseDto create(HuongDanDoAnDTO huongDanDoAnDTO) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(huongDanDoAnDTO.getDoAnId());
        Optional<DeTaiDoAn> deTaiDoAn = deTaiDoAnRepository.findById(huongDanDoAnDTO.getDeTaiDoAnId());
        Optional<GiaoVien> giaoVien = giaoVienRepository.findById(huongDanDoAnDTO.getGiaoVienId());
        if (giaoVien.isPresent() && deTaiDoAn.isPresent() && doAn.isPresent()) {
            if (deTaiDoAn.get().getDoAnId().equals(doAn.get().getId())) {
                HuongDanDoAn huongDanDoAn = toHuongDanDoAn.getDestination(huongDanDoAnDTO);
                huongDanDoAn.setIsDeleted(false);
                HuongDanDoAn result = huongDanDoAnRepository.save(huongDanDoAn);
                responseDto.setObject(result);
            } else {
                responseDto.setStatus(HttpStatus.NO_CONTENT.value());
                responseDto.setMessage("Lỗi đề tài !");
                responseDto.setObject(null);
            }
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto update(HuongDanDoAnDTO huongDanDoAnDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<HuongDanDoAn> huongDanDoAn = huongDanDoAnRepository.findById(huongDanDoAnDTO.getId());
        if (huongDanDoAn.isPresent()) {
            HuongDanDoAn huongDanDoAn1 = toHuongDanDoAn.getDestination(huongDanDoAn.get(), huongDanDoAnDTO);
            HuongDanDoAn result = huongDanDoAnRepository.save(huongDanDoAn1);
            HuongDanDoAnDTO huongDanDoAnDTO1 = toHuongDanDoAnDto.getDestination(result);
            responseDto.setObject(huongDanDoAnDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<HuongDanDoAn> huongDanDoAn = huongDanDoAnRepository.findById(id);
        if (huongDanDoAn.isPresent()) {
            huongDanDoAnRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto listNhomSinhVien(Integer pageIndex, Integer pageSize, Long giaoVienId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        searchReqDto.setQuery("N-giaoVienId=\"" + giaoVienId + "\"");
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }
    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<HuongDanDoAn> huongDanDoAn = huongDanDoAnRepository.findById(id);
        if (huongDanDoAn.isPresent()){
            huongDanDoAnRepository.delete(huongDanDoAn.get());
            responseDto.setObject(huongDanDoAn.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public Long count(Long id){
        Long huongDanDoAn = huongDanDoAnRepository.findByGiaoVienId(id).stream().count();
        return huongDanDoAn;
    }
}
