package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.*;
import com.java.project3.dto.*;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.KhoaRepository;
import com.java.project3.repository.LopRepository;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class KhoaService {
    @Autowired
    KhoaRepository khoaRepository;
    @Autowired
    GenIdService genIdService;
    @Autowired
    LopRepository lopRepository;

    JMapper<Khoa, KhoaDTO> toKhoa;
    JMapper<KhoaDTO, Khoa> toKhoaDto;

    public KhoaService() {
        this.toKhoa = new JMapper<>(Khoa.class, KhoaDTO.class);
        this.toKhoaDto = new JMapper<>(KhoaDTO.class, Khoa.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<Khoa> khoa = khoaRepository.findById(id);
        if (khoa.isPresent()) {
            KhoaDTO khoaDTO = toKhoaDto.getDestination(khoa.get());
            responseDto.setObject(khoaDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi đồ án !");
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto searchKhoa(Integer pageIndex, Integer pageSize, String search) {
        ResponseDto responseDto = new ResponseDto();
        if (pageIndex == null){
            pageIndex = 0;
        }
        if (pageSize == null){
            pageSize = 5;
        }
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize);
        KhoaDTO khoaDTO = new KhoaDTO();
        if (search == null) {
            search = "";
        }
        Page<Khoa> page = khoaRepository.searchkhoa(search, pageRequest);
        for (var khoa : page) {
            List<KhoaDTO> khoaDTOS = new ArrayList<>();
            khoaDTO = toKhoaDto.getDestination(khoa);
            khoaDTOS.add(khoaDTO);
            responseDto.setObject(prepareResponseForSearch(page.getTotalPages(), page.getNumber(), page.getTotalElements(), khoaDTOS));
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<Khoa> khoas = khoaRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<KhoaDTO> khoaDTOS = new ArrayList<>();
        for (var khoa : khoas) {
            KhoaDTO khoaDTO = toKhoaDto.getDestination(khoa);
            khoaDTO.setDem(lopRepository.countByKhoaId(khoa.getId()));
            khoaDTOS.add(khoaDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(khoas.getTotalPages(), khoas.getNumber(), khoas.getTotalElements(), khoaDTOS));

        return responseDto;
    }

    public ResponseDto listLop(Integer pageIndex, Integer pageSize) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        searchReqDto.setQuery(" ");
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage() -1 );

        responseDto = search(searchReqDto);

        responseDto = search(searchReqDto);
        return responseDto;
    }

    public ResponseDto create(KhoaDTO khoaDTO) {
        ResponseDto responseDto = new ResponseDto();
        Khoa khoa = toKhoa.getDestination(khoaDTO);
        khoa.setId(genIdService.nextId());
        khoa.setIsDeleted(false);
        Khoa result = khoaRepository.save(khoa);
        responseDto.setObject(result);
        return responseDto;
    }

    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<Khoa> khoa = khoaRepository.findById(id);
        if (khoa.isPresent()){
            khoaRepository.delete(khoa.get());
            responseDto.setObject(khoa.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto update(KhoaDTO khoaDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<Khoa> khoa = khoaRepository.findById(khoaDTO.getId());
        if (khoa.isPresent()) {
            Khoa khoas = toKhoa.getDestination(khoa.get(), khoaDTO);
            Khoa result = khoaRepository.save(khoas);
            KhoaDTO khoaDTO1 = toKhoaDto.getDestination(result);
            responseDto.setObject(khoaDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

}
