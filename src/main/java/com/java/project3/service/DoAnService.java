package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.*;
import com.java.project3.dto.DoAnDTO;
import com.java.project3.dto.GiaoVienDTO;
import com.java.project3.dto.SinhVienDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.*;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class DoAnService {
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    KhoaRepository khoaRepository;
    @Autowired
    LopRepository lopRepository;
    @Autowired
    GenIdService genIdService;
    @Autowired
    DeTaiDoAnRepository deTaiDoAnRepository;
    @Autowired
    HuongDanDoAnRepository huongDanDoAnRepository;

    JMapper<DoAn, DoAnDTO> toDoAn;
    JMapper<DoAnDTO, DoAn> toDoAnDto;

    public DoAnService() {
        this.toDoAn = new JMapper<>(DoAn.class, DoAnDTO.class);
        this.toDoAnDto = new JMapper<>(DoAnDTO.class, DoAn.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(id);
        if (doAn.isPresent()) {
            DeTaiDoAn deTaiDoAn = deTaiDoAnRepository.findByDoAnId(id);
            DoAnDTO doAnDTO = toDoAnDto.getDestination(doAn.get());
            doAnDTO.setDeTai(deTaiDoAn.getName());
            responseDto.setObject(doAnDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi đồ án !");
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<DoAn> doAns = doAnRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<DoAnDTO> doAnDTOS = new ArrayList<>();
        for (var doAn : doAns) {
            DeTaiDoAn deTaiDoAn = deTaiDoAnRepository.findByDoAnId(doAn.getId());
            DoAnDTO doAnDTO = toDoAnDto.getDestination(doAn);
            if (deTaiDoAn !=null) {
                doAnDTO.setDeTai(deTaiDoAn.getName());
            }
            doAnDTOS.add(doAnDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(doAns.getTotalPages(), doAns.getNumber(), doAns.getTotalElements(), doAnDTOS));

        return responseDto;
    }

    public ResponseDto create(DoAnDTO doAnDTO) {
        ResponseDto responseDto = new ResponseDto();
        Optional<Khoa> khoa = khoaRepository.findById(doAnDTO.getKhoaId());
//        Optional<Lop> lop = lopRepository.findById(doAnDTO.getLopId());
        if (khoa.isPresent() ) {
//            if (lop.get().getKhoaId().equals(khoa.get().getId())) {
                DoAn doAn = toDoAn.getDestination(doAnDTO);
                doAn.setId(genIdService.nextId());
                doAn.setTenKhoa(khoa.get().getName());
                doAn.setIsDeleted(false);
                DoAn result = doAnRepository.save(doAn);
                responseDto.setObject(result);

                DeTaiDoAn deTaiDoAn = new DeTaiDoAn();
                deTaiDoAn.setName(doAnDTO.getDeTai());
                deTaiDoAn.setIsDeleted(false);
                deTaiDoAn.setDoAnId(result.getId());
                deTaiDoAn.setId(genIdService.nextId());
                deTaiDoAnRepository.save(deTaiDoAn);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }

        return responseDto;
    }

    public ResponseDto update(DoAnDTO doAnDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<DoAn> doAn = doAnRepository.findById(doAnDTO.getId());
        if (doAn.isPresent()) {
            DoAn doAn1 = toDoAn.getDestination(doAn.get(), doAnDTO);
            doAn1.setThang(LocalDate.now().getMonth().getValue());
            DoAn result = doAnRepository.save(doAn1);
            DoAnDTO doAnDTO1 = toDoAnDto.getDestination(result);
            responseDto.setObject(doAnDTO1);

            DeTaiDoAn deTaiDoAn = deTaiDoAnRepository.findByDoAnId(result.getId());
            deTaiDoAn.setName(doAnDTO.getDeTai());
            deTaiDoAnRepository.save(deTaiDoAn);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto hoanThanh(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(id);
        if (doAn.isPresent()){
            doAn.get().setHoanThanh((short)1);
            doAnRepository.save(doAn.get());
            responseDto.setObject(doAn);
        }else {
            responseDto.setObject(null);
            responseDto.setStatus(400);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(id);
        if (doAn.isPresent()) {
            doAnRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto listGiaoVien(Integer pageIndex, Integer pageSize, Long lopId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        if (lopId == null){
            searchReqDto.setQuery(" ");

        }else {
            searchReqDto.setQuery("N-lopId=\"" + lopId + "\"");

        }
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }

    public ResponseDto listdoAn(Integer pageIndex, Integer pageSize, List<Long> doAnId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage() - 1);
        List<DoAnDTO> doAnDTOS = new ArrayList<>();
        DoAnDTO doAnDTO = new DoAnDTO();
        for (var i : doAnId){
            Optional<DoAn> doAn = doAnRepository.findById(i);
            if (doAn.isPresent()){
                doAnDTO = toDoAnDto.getDestination(doAn.get());
                doAnDTOS.add(doAnDTO);
            }
        }
        PageRequest pageRequest = PageRequest.of(searchReqDto.getPageIndex(), searchReqDto.getPageSize(),
                by(getOrders(searchReqDto.getSorts(), DEFAULT_PROP)));
        Page<DoAnDTO> doAnDTOS1 = new PageImpl<>(doAnDTOS, pageRequest, doAnDTOS.size());
        doAnDTOS1.stream().map(doAnDTO1 -> doAnDTOS).collect(Collectors.toList());
        responseDto.setObject(prepareResponseForSearch(doAnDTOS1.getTotalPages(), doAnDTOS1.getNumber(), doAnDTOS1.getTotalElements(), doAnDTOS));


        return responseDto;
    }

    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<DoAn> doAn = doAnRepository.findById(id);
        if (doAn.isPresent()){
            doAnRepository.delete(doAn.get());
            responseDto.setObject(doAn.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto searchDoAn(Integer pageIndex, Integer pageSize, String search) {
        ResponseDto responseDto = new ResponseDto();
        if (pageIndex == null){
            pageIndex = 0;
        }
        if (pageSize == null){
            pageSize = 5;
        }
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize);
        DoAnDTO doAnDTO = new DoAnDTO();
        if (search == null) {
            search = "";
        }
        Page<DoAn> page = doAnRepository.searchDoAn(search, pageRequest);
        for (var doan : page) {
            List<DoAnDTO> doAnDTOArrayList = new ArrayList<>();
            doAnDTO = toDoAnDto.getDestination(doan);
            doAnDTOArrayList.add(doAnDTO);
            responseDto.setObject(prepareResponseForSearch(page.getTotalPages(), page.getNumber(), page.getTotalElements(), doAnDTOArrayList));
        }
        return responseDto;
    }
    public ResponseDto dem1(Integer pageIndex, Integer pageSize) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        searchReqDto.setQuery("N-hoanThanh=\"" +1+"\"");
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }
}
