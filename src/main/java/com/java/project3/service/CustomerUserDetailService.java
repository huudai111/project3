package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.contant.RoleName;
import com.java.project3.domain.*;
import com.java.project3.dto.DeTaiDoAnDTO;
import com.java.project3.dto.UserDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.GiaoVienRepository;
import com.java.project3.repository.UserRepository;
import com.java.project3.repository.UserRoleRepository;
import com.java.project3.service.base.GenIdService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class CustomerUserDetailService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    GenIdService genIdService;
    @Autowired
    UserRoleRepository userRoleRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;

    JMapper<UserDTO, User> toUserDto;
    JMapper<User, UserDTO> toUser;

    public CustomerUserDetailService() {
        this.toUserDto = new JMapper<>(UserDTO.class, User.class);
        this.toUser = new JMapper<>(User.class, UserDTO.class);

    }
    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            UserDTO userDTO = toUserDto.getDestination(user.get());
            responseDto.setObject(userDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi nhóm sinh viên !");
            responseDto.setObject(null);
        }
        return responseDto;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User users = userRepository.findByEmail(username);
        if (users != null) {
            User user = users;
            List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();

            UserRole userRole = userRoleRepository.findByUserId(user.getId());
            if (userRole.getRoleId().equals(RoleName.getRoleIdByRoleName(RoleName.ADMIN))) {
                GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
                grantList.add(authority);
            }else {
                GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
                grantList.add(authority);
            }

            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), grantList);

            return userDetails;
        } else {
            new UsernameNotFoundException("Login fail!");
        }
        return null;
    }
//
//    private Collection<? extends GrantedAuthority> getAuthorities(
//            Collection<Role> roles) {
//        List<GrantedAuthority> authorities
//                = new ArrayList<>();
//        for (Role role: roles) {
//            authorities.add(new SimpleGrantedAuthority(role.getName()));
//            role.getPrivileges().stream()
//                    .map(p -> new SimpleGrantedAuthority(p.getName()))
//                    .forEach(authorities::add);
//        }
//
//        return authorities;
//    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();

        // Dùng hàm search (hero)
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<User> users = userRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<UserDTO> userDTOS = new ArrayList<>();
        for (var user : users) {
            userDTOS.add(toUserDto.getDestination(user));
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(users.getTotalPages(), users.getNumber(), users.getTotalElements(), userDTOS));

        return responseDto;
    }

    public ResponseDto signup(UserDTO userDTO) {
        ResponseDto responseDto = new ResponseDto();
        User user = userRepository.findByEmail(userDTO.getEmail());
        if (user != null) {
            responseDto.setStatus(400);
            responseDto.setMessage("Đã có tài khoản này rồi !");
            responseDto.setObject(null);
        } else {
            User user1 = toUser.getDestination(userDTO);
            user1.setPassword(user1.getPassword());
            user1.setId(genIdService.nextId());
            user1.setCapDo(false);
            userRepository.save(user1);
            responseDto.setObject(userDTO);

            UserRole userRole = new UserRole();
            userRole.setId(genIdService.nextId());
            userRole.setUserId(user1.getId());
            userRole.setRoleId(userDTO.getRoleId());
            userRoleRepository.save(userRole);

            GiaoVien giaoVien1 = new GiaoVien();
            giaoVien1.setId(genIdService.nextId());
            giaoVien1.setIsDeleted(false);
            giaoVien1.setUserId(user1.getId());
            giaoVien1.setEmail(user1.getEmail());
            giaoVien1.setName(user1.getUserName());
            giaoVien1.setKhoaId(userDTO.getKhoaId());
//            giaoVien1.setNgaySinh();
            GiaoVien result = giaoVienRepository.save(giaoVien1);
        }
        return responseDto;
    }

    public Long cout(){
        Long count = userRepository.findAll().stream().count();
        return count;
    }

    public ResponseDto login(UserDTO userDTO){
        ResponseDto responseDto = new ResponseDto();
        Optional<User> user = Optional.ofNullable(userRepository.findByEmailAndPassword(userDTO.getEmail(),userDTO.getPassword()));
        if (user.isPresent()){
            responseDto.setObject(user);
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }
}
