package com.java.project3.service;

import com.googlecode.jmapper.JMapper;
import com.java.project3.domain.*;
import com.java.project3.dto.GiaoVienDTO;
import com.java.project3.dto.HuongDanDoAnDTO;
import com.java.project3.dto.base.ResponseDto;
import com.java.project3.dto.base.SearchReqDto;
import com.java.project3.repository.*;
import com.java.project3.service.base.GenIdService;
import com.java.project3.utils.PageUltil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.java.project3.contant.Constants.DEFAULT_PROP;
import static com.java.project3.utils.SearchUtil.*;
import static org.springframework.data.domain.Sort.by;

@Service
public class GiaoVienService {
    @Autowired
    HuongDanDoAnRepository huongDanDoAnRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    DeTaiDoAnRepository deTaiDoAnRepository;
    @Autowired
    NhomRepository nhomRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    FileStorageService fileStorageService;
    @Autowired
    ResourceRepository resourceRepository;
    @Autowired
    GenIdService genIdService;

    JMapper<GiaoVien, GiaoVienDTO> toGiaoVien;
    JMapper<GiaoVienDTO, GiaoVien> toGiaoVienDto;

    public GiaoVienService() {
        this.toGiaoVien = new JMapper<>(GiaoVien.class, GiaoVienDTO.class);
        this.toGiaoVienDto = new JMapper<>(GiaoVienDTO.class, GiaoVien.class);
    }

    public ResponseDto findById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<GiaoVien> giaoVien = giaoVienRepository.findById(id);
        if (giaoVien.isPresent()) {
            GiaoVienDTO giaoVienDTO = toGiaoVienDto.getDestination(giaoVien.get());
            responseDto.setObject(giaoVienDTO);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi nhóm sinh viên !");
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto search(SearchReqDto reqDto) {
        ResponseDto responseDto = new ResponseDto();
        PageRequest pageRequest = PageRequest.of(reqDto.getPageIndex(), reqDto.getPageSize(),
                by(getOrders(reqDto.getSorts(), DEFAULT_PROP)));
        Page<GiaoVien> giaoViens = giaoVienRepository.findAll(createSpec(reqDto.getQuery()), pageRequest);

        // entity -> dto
        List<GiaoVienDTO> giaoVienDTOS = new ArrayList<>();
        for (var giaoVien : giaoViens) {
            GiaoVienDTO giaoVienDTO = toGiaoVienDto.getDestination(giaoVien);
            giaoVienDTOS.add(giaoVienDTO);
        }

        // set dto vào object của response
        responseDto.setObject(prepareResponseForSearch(giaoViens.getTotalPages(), giaoViens.getNumber(), giaoViens.getTotalElements(), giaoVienDTOS));

        return responseDto;
    }

    public ResponseDto create(GiaoVienDTO giaoVienDTO) {
        ResponseDto responseDto = new ResponseDto();
        Optional<GiaoVien> giaoVien = Optional.ofNullable(giaoVienRepository.findByEmail(giaoVienDTO.getEmail()));
        if (giaoVien.isPresent()) {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage("Lỗi đề tài !");
            responseDto.setObject(null);
        } else {
            GiaoVien giaoVien1 = toGiaoVien.getDestination(giaoVienDTO);
            giaoVien1.setId(genIdService.nextId());
            giaoVien1.setIsDeleted(false);
            GiaoVien result = giaoVienRepository.save(giaoVien1);
            responseDto.setObject(result);
        }

        return responseDto;
    }

    public ResponseDto update(GiaoVienDTO giaoVienDTO) {
        ResponseDto responseDto = new ResponseDto();

        Optional<GiaoVien> giaoVien = giaoVienRepository.findById(giaoVienDTO.getId());
        if (giaoVien.isPresent()) {
            GiaoVien giaoVien1 = toGiaoVien.getDestination(giaoVien.get(), giaoVienDTO);
            GiaoVien result = giaoVienRepository.save(giaoVien1);
            GiaoVienDTO giaoVienDTO1 = toGiaoVienDto.getDestination(result);
            responseDto.setObject(giaoVienDTO1);

        } else {
            responseDto.setStatus(HttpStatus.NOT_FOUND.value());
            responseDto.setMessage(HttpStatus.NOT_FOUND.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto deleteById(Long id) {
        ResponseDto responseDto = new ResponseDto();
        Optional<GiaoVien> giaoVien = giaoVienRepository.findById(id);
        if (giaoVien.isPresent()) {
            giaoVienRepository.deleteById(id);
            responseDto.setObject(id);
        } else {
            responseDto.setStatus(HttpStatus.NO_CONTENT.value());
            responseDto.setMessage(HttpStatus.NO_CONTENT.name());
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto listGiaoVien(Integer pageIndex, Integer pageSize, Long khoaId) {
        ResponseDto responseDto = new ResponseDto();
        SearchReqDto searchReqDto = new SearchReqDto();
        List<String> sort = new ArrayList<>();
        sort.add("createdAt");
        searchReqDto.setSorts(sort);
        if (khoaId == null){
            searchReqDto.setQuery(" ");
        }else {
            searchReqDto.setQuery("N-khoaId=\"" + khoaId + "\"");
        }
        com.java.project3.dto.Page page = PageUltil.setDefault(pageIndex, pageSize);
        searchReqDto.setPageSize(page.getPageSize());
        searchReqDto.setPageIndex(page.getCurrentPage()-1);

        responseDto = search(searchReqDto);
        return responseDto;
    }
    public ResponseDto delete(Long id){
        ResponseDto responseDto = new ResponseDto();
        Optional<GiaoVien> giaoVien = giaoVienRepository.findById(id);
        if (giaoVien.isPresent()){
            giaoVienRepository.delete(giaoVien.get());
            responseDto.setObject(giaoVien.get());
        }else {
            responseDto.setObject(null);
        }
        return responseDto;
    }

    public ResponseDto filePdf(MultipartFile[] files) {
        ResponseDto responseDto = new ResponseDto();
        if (files != null) {
            for (var item : files) {
                if (!item.getOriginalFilename().equals("")) {
                    String link = fileStorageService.storeMultipartFile(item);
                    Resource resource = new Resource();
                    resource.setId(genIdService.nextId());
                    resource.setContentType(item.getContentType());
                    resource.setFileName(item.getOriginalFilename());
                    resource.setLink(link);
//                    resource.setTargetId(temp.getId());
                    resourceRepository.save(resource);
                }
            }
        }
        return responseDto;
    }

    public Long demGiaoVien(){
        Long a = giaoVienRepository.findAll().stream().count();
        return a;
    }

    public ResponseDto all(){
        ResponseDto responseDto = new ResponseDto();
        List<GiaoVien> giaoVien = giaoVienRepository.findAll();
        responseDto.setObject(giaoVien);
        return responseDto;
    }
}
