package com.java.project3.utils.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;

public class ParseExcel {
    public static String getStringValueFromCell(Row row, int cellNo) {
        DataFormatter formatter = new DataFormatter();
        Cell cell = row.getCell(cellNo);
        if(cell == null){
            return "";
        }
        if(cell.getCellType() == CellType._NONE.getCode()){
            return "";
        }else if(cell.getCellType() == CellType.ERROR.getCode()){
            return "";
        }else if(cell.getCellType() == CellType.FORMULA.getCode()){
            return cell.toString().trim();
        }else{
            return formatter.formatCellValue(cell).trim();
        }
    }
}
