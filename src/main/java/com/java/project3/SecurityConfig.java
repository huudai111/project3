package com.java.project3;

import com.java.project3.service.CustomerUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import sun.security.util.Password;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    CustomerUserDetailService customerUserDetailService;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.csrf().disable();
//        http.authorizeRequests().antMatchers("/userInfo").access("hasAnyRole('ROLE_USER', 'ADMIN')");
//        http.authorizeRequests().antMatchers("/do-an").access("hasRole('ADMIN')");


//        http.authorizeRequests()
//////                .antMatchers("/home","/vendor/**","/js/**","/css/**","/fonts/**","/image/**").permitAll()
////                .antMatchers("/do-an").hasAnyAuthority("ADMIN")
////                .antMatchers("/callcenter").hasAnyAuthority("ADMIN", "ROLE_USER")
//                .antMatchers("/signup").permitAll()
//                .anyRequest().authenticated().and()
//                .formLogin().loginPage("/login").permitAll()
//                .defaultSuccessUrl("/home")
//                .failureUrl("/login?surcess=fail")
//                .loginProcessingUrl("/j_spring_security_check");
//        http.logout().logoutSuccessUrl("/home");
    }
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth)throws Exception{

        auth.userDetailsService(customerUserDetailService).passwordEncoder(passwordEncoder());
//        auth.inMemoryAuthentication().withUser("admin").password(String.valueOf(passwordEncoder())).roles("ADMIN");
    }
}
