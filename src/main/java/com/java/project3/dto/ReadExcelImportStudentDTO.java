package com.java.project3.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ReadExcelImportStudentDTO {
    private Integer rowNumber;
    private String name;
    private String email;
    private String phone;
    private String maSinhVien;
    private String tenKhoa;
    private String address;
    private String birthDate;
    private String gender;
    private List<Short> errorCells;
}
