package com.java.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateSinhVienDTO {
    @JMap
    private Long id;
    @JMap
    private Long khoaId;
    @JMap
    private Long lopId;
    @JMap
    private String maSv;
    @JMap
    private String name;
    @JMap
    private String tenLop;
    @JMap
    private String tenKhoa;
    @JMap
    private Short gioiTinh;
    @JMap
    private Date ngaySinh;
    @JMap
    private String sdt;
    @JMap
    private String email;
    @JMap
    private String diaChi;
}
