package com.java.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import com.java.project3.dto.base.AbstractBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NhomDTO extends AbstractBaseDTO {
    @JMap
    private String name;

    @JMap
    private Long deTaiDoAnId;

    @JMap
    private Long khoaId;

    @JMap
    private Long lopId;

    @JMap
    private Long doAnId;

}
