package com.java.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import com.java.project3.dto.base.AbstractBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DoAnDTO {
    @JMap
    private Long id;
    @JMap
    private Long khoaId;

    @JMap
    private String tenKhoa;

    @JMap
    private Long lopId;

    @JMap
    private Short hoanThanh;

    @JMap
    private Integer thang;

    @JMap
    private String tenLop;

    @JMap
    private String name;

    @JMap
    private String noiDung;

    @JMap
    private Date ngayBatDau;

    @JMap
    private Date ngayKetThuc;

    private String deTai;
    @JMap
    private LocalDate updatedAt;

    private Long giaoVienId;

}
