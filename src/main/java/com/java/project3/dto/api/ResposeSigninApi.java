package com.java.project3.dto.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ResposeSigninApi {
    private Long userId;
    private String username;
    private String roleName;
    private String token;
}
