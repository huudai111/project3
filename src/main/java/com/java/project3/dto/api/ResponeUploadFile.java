package com.java.project3.dto.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponeUploadFile {
    private Integer uploaded;
    private String fileName;
    private String url;
    private String fileType;
    private long size;
    private Error error;
}

