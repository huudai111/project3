package com.java.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import com.java.project3.dto.base.AbstractBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DanhGiaNhomDTO extends AbstractBaseDTO {
    @JMap
    private Long nhomId;

    @JMap
    private Long giaoVienId;
    @JMap
    private String name;

    @JMap
    private String noiDung;


    @JMap
    private String giaiDoan;


    @JMap
    private Short xep_loai;

}
