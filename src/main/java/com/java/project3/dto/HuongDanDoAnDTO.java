package com.java.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import com.java.project3.dto.base.AbstractBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HuongDanDoAnDTO extends AbstractBaseDTO {
    @JMap
    private Long doAnId;

    @JMap
    private Long giaoVienId;

    @JMap
    private Long deTaiDoAnId;

    @JMap
    private Short trangThai;

}
