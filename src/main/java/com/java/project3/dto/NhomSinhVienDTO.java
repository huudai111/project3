package com.java.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import com.java.project3.dto.base.AbstractBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NhomSinhVienDTO extends AbstractBaseDTO {
    @JMap
    private Long nhomId;

    @JMap
    private Long sinhVienId;

    @JMap
    private Long doAnId;

    @JMap
    private Long deTaiDoAnId;


    @JMap
    private Boolean truongNhom;


   private Long thanhVienId1;
    private Long thanhVienId2;
    private Long thanhVienId3;
    private String name;
    private Long dem;
    private String doAn;
}
