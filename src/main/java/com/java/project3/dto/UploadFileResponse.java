package com.java.project3.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileResponse {
    private Integer uploaded;
    private String fileName;
    private String url;
    private String fileType;
    private long size;


}
