package com.java.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import com.java.project3.dto.base.AbstractBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SinhVienDTO extends AbstractBaseDTO {
    @JMap
    private Long khoaId;
    @JMap
    private Long lopId;

    @JMap
    private String tenLop;

    @JMap
    private String tenKhoa;

    @JMap
    private String name;

    @JMap
    private Date ngaySinh;

    @JMap
    private Short gioiTinh;

    @JMap
    private String diaChi;

    @JMap
    private String sdt;

    @JMap
    private String email;

    @JMap
    private String maSv;


}
